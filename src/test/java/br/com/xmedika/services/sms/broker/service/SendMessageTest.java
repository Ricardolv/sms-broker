package br.com.xmedika.services.sms.broker.service;

import br.com.xmedika.services.sms.broker.SmsBrokerApp;
import br.com.xmedika.services.sms.broker.model.dto.MessageDTO;
import br.com.xmedika.services.sms.broker.util.DateTimeUtil;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.URI;
import org.apache.commons.httpclient.methods.GetMethod;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.io.IOException;
import java.text.ParseException;

import static br.com.xmedika.services.sms.broker.model.SmsConstants.*;
import static org.junit.Assert.assertNotNull;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SmsBrokerApp.class)
public class SendMessageTest {

    @Autowired
    SmsMessageService smsMessageService;

    @Autowired
    DateTimeUtil dateTimeUtil;

    /*===== TESTES TIPO LONG CODE =====*/

    //@Test
    public void sendLongSimple() throws IOException {

        String inputMessage = "SimpleDateFormat also supports localized date and time pattern strings. In these strings, the pattern letters described above may be replaced with other, r56677754332testerewqytrewqoiuyttrewwqwqqqaase";
        String inputNumbers = "31984260143";

        MessageDTO messageDTO = null;
        try {
            messageDTO = MessageDTO.convertJsonToObject(smsMessageService.sendLongSimple(inputNumbers, inputMessage));

        } catch (Exception e) {
            e.printStackTrace();
        }

        assertNotNull(messageDTO);
        System.out.println("status: " + messageDTO.getStatus() +
                " data: " + messageDTO.getData()
                .concat(" msg: ").concat(messageDTO.getMsg()));

        //status: 1 data: 82684617 msg: SUCESSO

    }

    @Test
    public void sendLongCallBack() throws IOException {
        MessageDTO messageDTO = null;

        try {
            messageDTO = MessageDTO.convertJsonToObject(smsMessageService.sendLongCallBack("31984260143", "Confirme sua consulta marcada, digitando SIM ou NÂO 16:34."));
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertNotNull(messageDTO);
        System.out.println("status: " + messageDTO.getStatus() +
                " data: " + messageDTO.getData()
                .concat(" msg: ").concat(messageDTO.getMsg()));

        //status: 1 data: 82837410 msg: SUCESSO
    }

    @Test
    public void sendLongscheduling() throws IOException {
        MessageDTO messageDTO = null;

        try {
            messageDTO = MessageDTO.convertJsonToObject(smsMessageService.sendLongScheduling("31984260143", "envio de uma mensagem agendada 15:22.", "6/9/2016", "16:53"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertNotNull(messageDTO);
        System.out.println("status: " + messageDTO.getStatus() +
                " data: " + messageDTO.getData()
                .concat(" msg: ").concat(messageDTO.getMsg()));

        //status: 1 data: 81538759 msg: SUCESSO
    }

    //@Test
    public void currentBalanceLong() throws IOException {
        MessageDTO messageDTO = null;

        try {
            messageDTO = MessageDTO.convertJsonToObject(smsMessageService.getLongCurrentBalance());
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertNotNull(messageDTO);
        System.out.println("Saldo long code: ".concat(messageDTO.getData()));
    }

    //@Test
    public void sendLongSeveralNumbers() throws IOException {
        String numbers = "31984260143, 31994927646";
        String message = "Envio de mensagem para varios numeros long code.";

        MessageDTO messageDTO = MessageDTO.convertJsonToObject(sendSeveralNumbers(LOCA_SMS_PATH, LOCA_SMS_PASSWORD, numbers, message));
        assertNotNull(messageDTO);

        System.out.println("status: " + messageDTO.getStatus() +
                " data: " + messageDTO.getData()
                .concat(" msg: ").concat(messageDTO.getMsg()));

        // return {"status":1,"data":"81099333","msg":"SUCESSO"}

    }

    //@Test
    public void getLongCampanha() throws IOException {

        String idCampanha = "81512552";
        MessageDTO messageDTO = new MessageDTO();

        try {
            messageDTO = MessageDTO.convertJsonToObject(smsMessageService.getLongCampanha(idCampanha));

        } catch (Exception e) {
            e.printStackTrace();
        }

        assertNotNull(messageDTO);

        String data = "\"488252521\";\"N\";\"06/09/2016 15:22:18\";\"06/09/2016 15:23:00\";\"06/09/2016 15:24:31\";\"SEM TÍTULO\";\"SUCESSO\";\"\";\"S\";\"VIVO\";\"31984260143\";\"31984260143\";\"envio de uma mensagem agendada 15:22.";
        String[] temp = MessageDTO.splitStringData(messageDTO.getData());

        System.out.println(temp[0]);
        System.out.println(temp[2]);
        System.out.println(temp[3]);
        System.out.println(temp[4]);




        // TODO tratar dados
        //data: "488252521";"N";"06/09/2016 15:22:18";"06/09/2016 15:23:00";"06/09/2016 15:24:31";"SEM TÍTULO";"SUCESSO";"";"S";"VIVO";"31984260143";"31984260143";"envio de uma mensagem agendada 15:22."
        /**
         * json:
         {"status":0,
         "data":"\"488252521\";
         \"N\";
         \"06/09/2016 15:22:18\";
         \"06/09/2016 15:23:00\";
         \"06/09/2016 15:24:31\";
         \"SEM TÍTULO\";
         \"SUCESSO\";
         \"\";
         \"S\";
         \"VIVO\";
         \"31984260143\";
         \"31984260143\";
         \"envio de uma mensagem agendada 15:22.\"",
         "msg":null,
         "jobDate":null,
         "jobTime":null}
         */
    }

    /*===== TESTES TIPO SHORT CODE =====*/

    //@Test
    public void sendShortSimple() throws IOException {
        MessageDTO messageDTO = null;

        String inputMessage = "SimpleDateFormat also supports localized date and time pattern strings. In these strings, the pattern letters described above may be replaced with other, r";
        String inputNumbers = "31984260143";

        try {
            messageDTO = MessageDTO.convertJsonToObject(smsMessageService.sendShortSimple(inputNumbers, inputMessage));

        } catch (Exception e) {
            e.printStackTrace();
        }

        assertNotNull(messageDTO);
        System.out.println("status: " + messageDTO.getStatus() +
                " data: " + messageDTO.getData()
                .concat(" msg: ").concat(messageDTO.getMsg()));
        //status: 1 data: 366990 msg: SUCESSO
    }

    //@Test
    public void sendShortCallBack() throws IOException {
        try {
            smsMessageService.sendShortCallBack("31984260143", "envio de uma mensagem call back short code.");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //@Test
    public void sendShortscheduling() throws IOException {
        try {
            smsMessageService.sendShortScheduling("31984260143", "envio de uma mensagem agendada short code.", "30/08/2016", "17:50");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //@Test
    public void currentBalanceShort() throws IOException {
        MessageDTO messageDTO = null;

        try {
            messageDTO = MessageDTO.convertJsonToObject(smsMessageService.getShortCurrentBalance());

        } catch (Exception e) {
            e.printStackTrace();
        }
        assertNotNull(messageDTO);
        System.out.println("Saldo short code: ".concat(messageDTO.getData()));
    }

    //@Test
    public void sendShortSeveralNumbers() throws IOException {
        String numbers = "31984260143, 31994927646";
        String message = "Envio de mensagem para varios numeros short code.";

        MessageDTO messageDTO = MessageDTO.convertJsonToObject(sendSeveralNumbers(PLATAFORM_SMS_PATH, PLATAFORM_SMS_PASSWORD, numbers, message));
        assertNotNull(messageDTO);

        System.out.println("status: " + messageDTO.getStatus() +
                " data: " + messageDTO.getData()
                .concat(" msg: ").concat(messageDTO.getMsg()));

        // return {"status":1,"data":"81099333","msg":"SUCESSO"}
    }

    //@Test
    public void getShortCampanha() throws IOException {

        String idCampanha = "81335319";
        MessageDTO messageDTO = null;

        try {
            messageDTO = MessageDTO.convertJsonToObject(smsMessageService.getShortCampanha(idCampanha));

        } catch (Exception e) {
            e.printStackTrace();
        }

        assertNotNull(messageDTO);
        System.out.println(" data: " + messageDTO.getData());

        // TODO tratar dados
        //{"status":0,"data":"\"485857992\";\"N\";\"30/08/2016 18:01:17\";\"\";\"31/08/2016 07:00:10\";\"SEM TÍTULO\";\"SUCESSO\";\"\";\"S\";\"VIVO\";\"31984260143\";\"31984260143\";\"envio de uma mensagem para varios numeros, desculpa ai II.\"\n\"485857993\";\"N\";\"30/08/2016 18:01:17\";\"\";\"30/08/2016 21:20:41\";\"SEM TÍTULO\";\"SUCESSO\";\"\";\"S\";\"TIM\";\"31994927646\";\"31994927646\";\"envio de uma mensagem para varios numeros, desculpa ai II.\"","msg":null}

    }

    //@Test
    public void returnLongCallback() throws ParseException {
        //367117;31984260143;;2016-09-05 13:58:01;SUCESSO;
        //367117;31984260143;OK;2016-09-05 14:10:51;RECEBIDO SUCESSO;
        Object[] dataCallback = {367117, "31984260143","", "2016-09-05 13:58:01","2016-09-05 13:58:01",""};

        Integer codigoCampanha = (Integer) dataCallback[0];
        String celular = (String) dataCallback[1];
        String resposta = (String) dataCallback[2];
        String dataResposta = (String) dataCallback[3];
        String status = (String) dataCallback[4];
        String internoKey = (String) dataCallback[5];


        //{"status":1,"data":"81440638","msg":"SUCESSO"}

        //81434354;31984260143;;2016-09-05 15:25:02;SUCESSO;
        //81442423;31984260143;SIM;2016-09-05 16:39:10;RECEBIDO SUCESSO;
    }

    private String getCampanha(final String inputPath, final String inputPassword, final String inputId, final String inputNumbers) throws IOException {

        HttpClient client = new HttpClient();
        HttpMethod method = new GetMethod();

        method.setURI(new URI(HTTP.concat(HOST), true));
        method.setPath(inputPath);

        NameValuePair action = new NameValuePair(DESC_ACTION, GET_STATUS_CAMPANHA);
        NameValuePair lgn = new NameValuePair(DESC_LGN, USER_NAME);
        NameValuePair pwd = new NameValuePair(DESC_PASSWORD, inputPassword);
        NameValuePair numbers = new NameValuePair(DESC_NUMBERS, inputNumbers);
        NameValuePair campanha = new NameValuePair("id", inputId);

        NameValuePair[] query = {action, lgn, pwd, numbers, campanha};

        String queryStr = org.apache.commons.httpclient.util.EncodingUtil.formUrlEncode(query, "UTF-8");
        method.setQueryString(queryStr);

        int httpStatus = client.executeMethod(method);
        return method.getResponseBodyAsString();
    }

    public String sendSeveralNumbers(final String inputPath, final String inputPassword, final String inputMessage, final String inputNumbers) throws IOException {
        HttpClient client = new HttpClient();
        HttpMethod method = new GetMethod();

        method.setURI(new URI(HTTP.concat(HOST), true));
        method.setPath(inputPath);

        NameValuePair action = new NameValuePair(DESC_ACTION, SEND_MESSAGE);
        NameValuePair lgn = new NameValuePair(DESC_LGN, USER_NAME);
        NameValuePair pwd = new NameValuePair(DESC_PASSWORD, inputPassword);
        NameValuePair msg = new NameValuePair(DESC_MESSAGE, inputMessage);
        NameValuePair numbers = new NameValuePair(DESC_NUMBERS, inputNumbers);

        NameValuePair[] query = {action, lgn, pwd, msg, numbers};

        String queryStr = org.apache.commons.httpclient.util.EncodingUtil.formUrlEncode(query, "UTF-8");
        method.setQueryString(queryStr);

        int httpStatus = client.executeMethod(method);
        return method.getResponseBodyAsString();
    }
}
