package br.com.xmedika.services.sms.broker;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SmsBrokerApp.class)
@WebAppConfiguration
public class SmsBrokerAppTests {

	@Test
	public void contextLoads() {
	}

}
