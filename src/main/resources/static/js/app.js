(function () {
    angular.module(SP_APP.name, [
        'ui.router',                    // Routing
        'ngTouch',
        'ngSanitize',
        'ngAnimate',
        'ui.bootstrap',                 // Ui Bootstrap
        'datatables',                   //DataTables
        'ui.tree',                      //UI Tree
        'checklist-model',              //Checklist model
        'datePicker',
        'angular-confirm',
        'toaster',
        'pascalprecht.translate',     	// Angular Translate
        'ui.mask',
        'ui.utils.masks',
        'idf.br-filters',
        'ui.calendar',                  //fullcalendar
        'angular-ladda',
        'oc.lazyLoad'
    ])
})();
