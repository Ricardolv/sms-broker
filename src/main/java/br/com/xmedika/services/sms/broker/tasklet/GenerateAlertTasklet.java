package br.com.xmedika.services.sms.broker.tasklet;

import br.com.xmedika.services.sms.broker.service.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GenerateAlertTasklet implements Tasklet {

    private static Logger logger = LoggerFactory.getLogger(GenerateAlertTasklet.class);

    @Autowired
    private MessageService messageService;

    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
        logger.debug("############################# Initializing Alert Tasklet  #############################");

         messageService.alertNotResponseQuery();

        logger.debug("############################# End of  Alert  Execution #############################");
        return null;
    }
}
