@TypeDefs({
        @TypeDef(name = "TypeSend", typeClass = EnumUserType.class, parameters = {
                @Parameter(name = "enumClass", value = "br.com.xmedika.services.sms.broker.model.TypeSendEnum")}),
        @TypeDef(name = "typeSendMessage", typeClass = EnumUserType.class, parameters = {
                @Parameter(name = "enumClass", value = "br.com.xmedika.services.sms.broker.model.SendMessageEnum")})

})

package br.com.xmedika.services.sms.broker.model;

import br.com.socialpro.orm.EnumUserType;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;