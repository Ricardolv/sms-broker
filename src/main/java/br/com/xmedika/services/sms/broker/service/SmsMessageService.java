package br.com.xmedika.services.sms.broker.service;


public interface SmsMessageService {

    String sendLongSimple(String numbers, String message) throws Exception;
    String sendLongCallBack(String numbers, String message) throws Exception;
    String sendLongScheduling(String numbers, String message, String jobDate, String jobTime) throws Exception;
    String getLongCurrentBalance() throws Exception;
    String getLongCampanha(String idCampanha) throws Exception;

    String sendShortSimple(String numbers, String message) throws Exception;
    String sendShortCallBack(String numbers, String message) throws Exception;
    String sendShortScheduling(String numbers, String message, String jobDate, String jobTime) throws Exception;
    String getShortCurrentBalance() throws Exception;
    String getShortCampanha(String idCampanha) throws Exception;

}
