package br.com.xmedika.services.sms.broker;

import br.com.socialpro.AppManifest;
import org.springframework.beans.factory.annotation.Value;

import java.util.*;

public class Manifest implements AppManifest {

    @Value("${base.url}")
    String baseUrl;

    @Override
    public String getName() {
        return "SMS Broker";
    }

    @Override
    public String getLabel() {
        return "SMS Broker";
    }

    @Override
    public String getDescription() {
        return "SMS Broker xMedika";
    }

    @Override
    public String getBaseURL() {
        return baseUrl;
    }

    @Override
    public List<String> getProjetctPackages() {
        List<String> basePackages = new ArrayList<>();
        basePackages.add("br.com.xmedika.services.sms.broker");
        return basePackages;
    }

    @Override
    public Map<String, Locale> getLocales() {
        Map<String, Locale> result = new HashMap<>();

        result.put("en_US", new Locale("en", "US"));
        result.put("pt_BR", new Locale("pt", "BR"));

        return result;
    }

    @Override
    public Locale getDefaultLocale() {
        return new Locale("pt", "BR");
    }

    @Override
    public String getDefaultTimezone() {
        return "America/Sao_Paulo";
    }
}

