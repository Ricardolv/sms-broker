package br.com.xmedika.services.sms.broker.service.impl;

import br.com.socialpro.service.impl.AbstractCrudService;
import br.com.xmedika.services.sms.broker.model.Account;
import br.com.xmedika.services.sms.broker.model.Domain;
import br.com.xmedika.services.sms.broker.model.QDomain;
import br.com.xmedika.services.sms.broker.repository.DomainRepository;
import br.com.xmedika.services.sms.broker.service.AccountService;
import br.com.xmedika.services.sms.broker.service.DomainService;
import com.querydsl.core.types.dsl.EntityPathBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DomainServiceImpl extends AbstractCrudService<Domain, Long, DomainRepository> implements DomainService {

    @Autowired
    DomainRepository domainRepository;

    @Autowired
    private AccountService accountService;

    @Override
    protected DomainRepository getRepository() {
        return domainRepository;
    }

    @Override
    public EntityPathBase<Domain> entityPath() {
        return QDomain.domain;
    }

    @Override
    public Account getAccount(Long idDomain, Long idAccountClient) {
        return accountService.findOneByDomainAndByAccountClient(idDomain, idAccountClient);
    }

    @Override
    public Account addCreditAccount(Long idDomain, Long accountId, Integer value) {
        return accountService.addCredit(idDomain, accountId, value);
    }
}
