package br.com.xmedika.services.sms.broker.tasklet;

import br.com.xmedika.services.sms.broker.model.dto.MessageDTO;
import br.com.xmedika.services.sms.broker.model.Message;
import br.com.xmedika.services.sms.broker.model.MessageLog;
import br.com.xmedika.services.sms.broker.service.*;
import br.com.xmedika.services.sms.broker.util.DateTimeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SendMessageLogTasklet implements Tasklet {

    private static Logger logger = LoggerFactory.getLogger(SendMessageLogTasklet.class);

    @Autowired
    MessageLogService messageLogService;

    @Autowired
    SmsMessageService smsMessageService;

    @Autowired
    MessageService messageService;

    @Autowired
    AccountService accountService;

    @Autowired
    DateTimeUtil dateTimeUtil;

    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
        logger.debug("#############################PROCESSING SEND MESSAGE LOG#############################");

            sendMessageLog();

        logger.debug("#############################PROCESSING SEND MESSAGE LOG#############################");
        return null;
    }

    private void sendMessageLog() {
        getMessagesLogs().stream().forEach(m -> sendMessage(m));
    }

    private List<MessageLog> getMessagesLogs() {
        return messageLogService.messageLogNotTypeErrorNotLimitThree();
    }

    /**
     *
     * @param messageLog
     */
    private void sendMessage(MessageLog messageLog) {

        Message entity = new Message();
        entity.setBody(messageLog.getBody());
        entity.setMobileNumber(messageLog.getMobileNumber());

        String[] data = {messageLog.getMobileNumber(), messageLog.getBody(),
                        dateTimeUtil.convertDateTimeToStringFormatDDMMYYYY(messageLog.getJobDateTime()),
                        dateTimeUtil.convertDateTimeToStringFormatHHmm(messageLog.getJobDateTime())
                    };
        Sms sendMessage = messageLog.getTypeSendMessage().sendMessage(data);

        String returnSend = sendMessage.returnSendSms(entity);

        try {
            MessageDTO messageDTO = MessageDTO.convertJsonToObject(returnSend);

            if (null != messageDTO && messageDTO.getStatus() == 1) {
                finishingProcess(messageDTO, entity, messageLog);
            } else {
                saveMessageLog(messageLog);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param entity
     * @param messageDTO
     * @return String
     * @throws Exception
     */
    private void finishingProcess(MessageDTO messageDTO, Message entity, MessageLog messageLog) throws Exception {

        entity.setStatus(messageDTO.getStatus());
        entity.setData(messageDTO.getData());
        entity.setMsg(messageDTO.getMsg());

        messageService.save(entity);
        messageLogService.delete(messageLog);
    }

    /**
     *
     * @param messageLog
     * @throws Exception
     */
    private void saveMessageLog(MessageLog messageLog) throws Exception {
        messageLog.setLimitSend(messageLog.getLimitSend() + 1);
        messageLogService.save(messageLog);
    }

}
