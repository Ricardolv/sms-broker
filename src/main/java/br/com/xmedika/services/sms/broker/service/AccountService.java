package br.com.xmedika.services.sms.broker.service;


import br.com.socialpro.service.CrudService;
import br.com.xmedika.services.sms.broker.model.Account;
import br.com.xmedika.services.sms.broker.model.Domain;

import java.util.List;

public interface AccountService extends CrudService<Account, Long> {

    Account lowInTheBalance(Long accountId, Integer credit);
    boolean noCreditLimit(Integer value);

    Account addCredit(Long accountId, Integer value);
    Account addCredit(Long domainId, Long accountId, Integer value);

    Account findOneByAccountClient(Long id);
    Account findOneByDomainAndByAccountClient(Long idDomain, Long id);

    List<Account> listAccountByDomain(Long idDomain);

    boolean thereIsNotAccount(Long idDomain, Long idAccount);

    boolean thereIsAccount(Long idDomain, Long idAccount);

    Domain getDomain(Long domainId);
}
