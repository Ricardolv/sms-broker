package br.com.xmedika.services.sms.broker.service.impl;

import br.com.socialpro.service.impl.AbstractCrudService;
import br.com.xmedika.services.sms.broker.model.*;
import br.com.xmedika.services.sms.broker.repository.MessageLogRepository;
import br.com.xmedika.services.sms.broker.service.MessageLogService;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.jpa.impl.JPAQuery;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MessageLogServiceImpl extends AbstractCrudService<MessageLog, Long, MessageLogRepository> implements MessageLogService {

    @Autowired
    MessageLogRepository messageLogRepository;

    @Override
    protected MessageLogRepository getRepository() {
        return messageLogRepository;
    }

    @Override
    public EntityPathBase<MessageLog> entityPath() {
        return QMessageLog.messageLog;
    }

    @Override
    public List<MessageLog> messageLogNotTypeErrorNotLimitThree() {
        JPAQuery<MessageLog> query = createQuery();
        query.where(QMessageLog.messageLog.isMessageError.isFalse()
                    .and(QMessageLog.messageLog.limitSend.isNotNull())
                    .and(QMessageLog.messageLog.limitSend.lt(3)));

        return query.fetch();
    }

    @Override
    public void createMessageError(Exception e, SendMessageEnum sendMessageEnum) {
        MessageLog messageLog = new MessageLog();
        messageLog.setCreatedTime(DateTime.now());
        messageLog.setTypeSendMessage(sendMessageEnum);
        messageLog.setBody(e.getMessage());
        messageLog.setMessageError(true);
        messageLogRepository.save(messageLog);
    }

    @Override
    public void createMessageLog(Message entity, TypeSendEnum typeSendEnum, SendMessageEnum sendMessageEnum) {
        MessageLog messageLog = new MessageLog();
        messageLog.setCreatedTime(DateTime.now());
        messageLog.setStatus(entity.getStatus());
        messageLog.setData(entity.getData());
        messageLog.setMsg(entity.getMsg());
        messageLog.setMobileNumber(entity.getMobileNumber());
        messageLog.setBody(entity.getBody());
        messageLog.setJobDateTime(entity.getJobDateTime());
        messageLog.setTypeSend(typeSendEnum);
        messageLog.setTypeSendMessage(sendMessageEnum);
        messageLogRepository.save(messageLog);
    }
}
