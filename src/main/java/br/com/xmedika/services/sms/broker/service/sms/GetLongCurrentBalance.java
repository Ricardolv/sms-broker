package br.com.xmedika.services.sms.broker.service.sms;

import br.com.socialpro.utils.ApplicationContextProvider;
import br.com.xmedika.services.sms.broker.model.dto.MessageDTO;
import br.com.xmedika.services.sms.broker.model.Message;
import br.com.xmedika.services.sms.broker.model.SendMessageEnum;
import br.com.xmedika.services.sms.broker.service.MessageLogService;
import br.com.xmedika.services.sms.broker.service.Sms;
import br.com.xmedika.services.sms.broker.service.SmsMessageService;

import java.io.IOException;

import static br.com.xmedika.services.sms.broker.model.SmsConstants.RETURN_NOK;

public class GetLongCurrentBalance implements Sms {

    private SmsMessageService smsMessageService;
    private MessageLogService messageLogService;


    public GetLongCurrentBalance() {
        this.smsMessageService = ApplicationContextProvider.getApplicationContext().getBean(SmsMessageService.class);
        this.messageLogService = ApplicationContextProvider.getApplicationContext().getBean(MessageLogService.class);
    }

    @Override
    public String returnSendSms(Message entity) {

        try {
            return processesBusinessRuleData(getSmsMessageService().getLongCurrentBalance());
        } catch (Exception e) {
            //Exception: Caso ocorra erro no envio do sms servico smsMessageService.getLongCurrentBalance()
            getMessageLogService().createMessageError(e, SendMessageEnum.GET_LONG_CURRENT_BALANCE);

            return  RETURN_NOK;
        }
    }

    /**
     *
     * @param returnSend
     * @return String
     */
    private String processesBusinessRuleData(String returnSend) {

        try {
            MessageDTO messageDTO = MessageDTO.convertJsonToObject(returnSend);
            return messageDTO.getData();

        } catch (IOException e) {
            //IOException: Caso ocorra erro de conversao convertJsonToObject
            getMessageLogService().createMessageError(e, SendMessageEnum.GET_LONG_CURRENT_BALANCE);
            return  RETURN_NOK;
        }
    }

    public SmsMessageService getSmsMessageService() {
        return smsMessageService;
    }

    public MessageLogService getMessageLogService() {
        return messageLogService;
    }
}
