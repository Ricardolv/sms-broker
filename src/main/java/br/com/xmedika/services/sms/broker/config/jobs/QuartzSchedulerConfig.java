package br.com.xmedika.services.sms.broker.config.jobs;

import br.com.socialpro.service.JobLauncherService;
import br.com.socialpro.transaction.SPTransactionManager;
import br.com.xmedika.services.sms.broker.config.DataSourceConfig;
import org.quartz.JobDataMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.support.ManagedProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import javax.sql.DataSource;
import java.util.Date;
import java.util.Properties;

/**
 * Configuação de execução de tarefas agendadas
 */
@Configuration
@Import(DataSourceConfig.class)
public class QuartzSchedulerConfig {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private SPTransactionManager spTransactionManager;

    protected String sendMessageLog = "0 0/30 * 1/1 * ? *";

    protected String generateAlert = "0 0/30 * 1/1 * ? *";


    /**
     * Componente principal do Quartz responsável pelo registro dos gatilhos que
     * disparam as tarefas agendadas
     *
     * @return schedulerFactoryBean
     */
    @Bean
    public SchedulerFactoryBean quartzScheduler() {


        Properties quartzProperties = new ManagedProperties();

        quartzProperties.setProperty("org.quartz.scheduler.instanceName", "imed-scheduler");
        quartzProperties.setProperty("org.quartz.scheduler.skipUpdateCheck", "true");
        quartzProperties.setProperty("org.quartz.jobStore.class", "org.quartz.impl.jdbcjobstore.JobStoreTX");
        quartzProperties.setProperty("org.quartz.jobStore.driverDelegateClass", "org.quartz.impl.jdbcjobstore.PostgreSQLDelegate"); //TODO: Colocar na configuração

        SchedulerFactoryBean schedulerFactoryBean = new SchedulerFactoryBean();

        schedulerFactoryBean.setTransactionManager(spTransactionManager);
        schedulerFactoryBean.setDataSource(dataSource);
        schedulerFactoryBean.setOverwriteExistingJobs(true);
        schedulerFactoryBean.setQuartzProperties(quartzProperties);

        schedulerFactoryBean.setTriggers(
                //triggerSendMessageLogJob().getObject(),
                triggerGenerateAlertJob().getObject()
        );

        return schedulerFactoryBean;
    }

    /**
     * Gatilho que dispara a execução da rotina de gerar os todo's automaticamente
     *
     * @return
     */
    @Bean
    public CronTriggerFactoryBean triggerSendMessageLogJob() {

        CronTriggerFactoryBean cronTriggerFactoryBean = new CronTriggerFactoryBean();
        cronTriggerFactoryBean.setJobDetail(sendMessageLogJobDetail().getObject());
        cronTriggerFactoryBean.setCronExpression(sendMessageLog);
        cronTriggerFactoryBean.setGroup("send-message-error-quartz");

        return cronTriggerFactoryBean;
    }

    /**
     * Rotina de gerar os todo's automaticamente
     */
    @Bean
    public JobDetailFactoryBean sendMessageLogJobDetail() {

        JobDataMap jobData = new JobDataMap();

        jobData.put("jobName", "sendMessageLogJob");
        jobData.put("startTime", new Date());

        JobDetailFactoryBean jobDetailFactory = new JobDetailFactoryBean();
        jobDetailFactory.setJobClass(JobLauncherService.class);
        jobDetailFactory.setGroup("send-message-error-quartz");
        jobDetailFactory.setJobDataMap(jobData);

        jobDetailFactory.setDurability(true);

        return jobDetailFactory;
    }

    /**
     * Gatilho que dispara a execução da rotina de gerar  alertas automaticamente
     *
     * @return
     */
    @Bean
    public CronTriggerFactoryBean triggerGenerateAlertJob() {

        CronTriggerFactoryBean cronTriggerFactoryBean = new CronTriggerFactoryBean();
        cronTriggerFactoryBean.setJobDetail(generateAlertJobDetail().getObject());
        cronTriggerFactoryBean.setCronExpression(generateAlert);
        cronTriggerFactoryBean.setGroup("send-message-error-quartz");

        return cronTriggerFactoryBean;
    }

    /**
     * Rotina de gerar alertas automaticamente
     */
    @Bean
    public JobDetailFactoryBean generateAlertJobDetail() {

        JobDataMap jobData = new JobDataMap();

        jobData.put("jobName", "generateAlertJob");
        jobData.put("startTime", new Date());

        JobDetailFactoryBean jobDetailFactory = new JobDetailFactoryBean();
        jobDetailFactory.setJobClass(JobLauncherService.class);
        jobDetailFactory.setGroup("send-message-error-quartz");
        jobDetailFactory.setJobDataMap(jobData);

        jobDetailFactory.setDurability(true);

        return jobDetailFactory;
    }
}
