package br.com.xmedika.services.sms.broker.service;

import br.com.xmedika.services.sms.broker.model.Message;

public interface Sms {

    /**
     * @param entity
     * @return String
     */
    String returnSendSms(Message entity);
}
