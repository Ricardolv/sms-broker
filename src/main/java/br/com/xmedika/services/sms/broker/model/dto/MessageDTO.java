package br.com.xmedika.services.sms.broker.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import org.joda.time.DateTime;

import java.io.IOException;

@Data
public class MessageDTO {

    /*CAPTURAR DADOS DO API DO FORNECEDOR*/
    @JsonProperty("status")
    private Integer status;

    @JsonProperty("data")
    private String data;

    @JsonProperty("msg")
    private String msg;

    @JsonProperty("jobDate")
    private String jobDate;

    @JsonProperty("jobTime")
    private String jobTime;

    /*CAPTURAR DADOS DA API SMS-BROKER-CLIENT*/

    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss", timezone="UTC")
    @JsonProperty("startEvent")
    private DateTime startEvent;

    @JsonProperty("mobileNumber")
    private String mobileNumber;

    @JsonProperty("text")
    private String text;

    @JsonProperty("idDomain")
    private Long idDomain;

    @JsonProperty("idEvent")
    private Long idEvent;

    @JsonProperty("idAccountClient")
    private Long idAccountClient;

    //opcao - gerar um alerta para confirmacao por telefone se o paciente não responder o SMS
    @JsonProperty("isGenerateAlertPhone")
    private Boolean isGenerateAlertPhone;

    //gerar um alerta para confirmacao por telefone se o paciente nao responder o SMS - horas
    @JsonProperty("hoursGenerateAlertPhone")
    private Short hoursGenerateAlertPhone;

    //resposta do cliente
    @JsonProperty("customerResponse")
    private String customerResponse;

    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss", timezone="UTC")
    @JsonProperty("sendStart")
    private DateTime sendStart;

    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss", timezone="UTC")
    @JsonProperty("sendEnd")
    private DateTime sendEnd;

    /**
     * Metodo responsavel por converter objet Java para JSON
     * @param messageDTO
     * @return String
     */
    public static String convertObjectToJson(MessageDTO messageDTO) throws IOException {
        return new ObjectMapper().writeValueAsString(messageDTO);
    }

    /**
     * Metodo responsavel por converter JSON para objet Java
     * @param message
     * @return MessageDTO
     */
    public static MessageDTO convertJsonToObject(String message) throws IOException {
        return new ObjectMapper().readValue(message, MessageDTO.class);
    }

    /**
     * Metodo responsavel por separar String de ponto e virgula
     * @param data
     * @return String[]
     */
    public static String[] splitStringData(String data) {
        return data.split(";");
    }

}