package br.com.xmedika.services.sms.broker.service.impl;

import br.com.xmedika.services.sms.broker.service.SmsMessageService;
import org.apache.commons.httpclient.*;
import org.apache.commons.httpclient.methods.GetMethod;
import org.springframework.stereotype.Service;

import static br.com.xmedika.services.sms.broker.model.SmsConstants.*;

@Service
public class SmsMessageServiceImpl implements SmsMessageService {

    final NameValuePair login = new NameValuePair(DESC_LGN, USER_NAME);
    final NameValuePair actionSendMessage = new NameValuePair(DESC_ACTION, SEND_MESSAGE);
    final NameValuePair actionGetBalance = new NameValuePair(DESC_ACTION, GET_BALANCE);
    final NameValuePair actionGetStatus = new NameValuePair(DESC_ACTION, GET_STATUS_CAMPANHA);

    /**
     * Method responsible to send simple type long code
     * @param message
     * @param numbers
     * @return String
     * @throws Exception
     */
    @Override
    public String sendLongSimple(String numbers, String message) throws Exception {
        return sendMessage(LOCA_SMS_PATH, mountNameValuePairSendSimple(LOCA_SMS_PASSWORD, message, numbers));
    }

    /**
     * Method responsible to perform callback type long code
     * @param message
     * @param numbers
     * @throws Exception
     */
    @Override
    public String sendLongCallBack(String numbers, String message) throws Exception {
       return sendMessage(LOCA_SMS_PATH,
                mountNameValuePairCallBack(LOCA_SMS_PASSWORD, message, numbers, LOCA_SMS_URL_CALLBACK));
    }

    /**
     * TODO ainda precisa saber como obter confirmacao de disparo
     * Method responsible to perform Scheduling type long code
     * @param message
     * @param numbers
     * @param jobDate
     * @param jobTime
     * @return String
     * @throws Exception
     */
    @Override
    public String sendLongScheduling(String numbers, String message, String jobDate, String jobTime) throws Exception {
          return sendMessage(LOCA_SMS_PATH,
                    mountNameValuePairScheduling(LOCA_SMS_PASSWORD, message, numbers, jobDate, jobTime));
    }

    /**
     * Method responsible of get current balance type long code
     * @return String
     * @throws Exception
     */
    @Override
    public String getLongCurrentBalance() throws Exception {
        return sendMessage(LOCA_SMS_PATH,
                mountNameValuePairCurrentBalance(LOCA_SMS_PASSWORD));
    }

    /**
     * Method responsible of get campanha type long code
     * @param idCamapnha
     * @return String
     * @throws Exception
     */
    @Override
    public String getLongCampanha(String idCamapnha) throws Exception {
        return sendMessage(LOCA_SMS_PATH, mountNameValuePairCampanha(LOCA_SMS_PASSWORD, idCamapnha));
    }


    /**
     * Method responsible to send simple type short code
     * @param message
     * @param numbers
     * @return String
     * @throws Exception
     */
    @Override
    public String sendShortSimple(String numbers, String message) throws Exception {
        return sendMessage(PLATAFORM_SMS_PATH, mountNameValuePairSendSimple(PLATAFORM_SMS_PASSWORD, message, numbers));
    }

    /**
     * Method responsible to perform callback type short code
     * @param message
     * @param numbers
     * @throws Exception
     */
    @Override
    public String sendShortCallBack(String numbers, String message) throws Exception {
        return sendMessage(PLATAFORM_SMS_PATH,
                mountNameValuePairCallBack(PLATAFORM_SMS_PASSWORD, message, numbers, PLATAFORM_SMS_URL_CALLBACK));
    }

    /**
     * TODO ainda precisa saber como obter confirmacao de disparo
     * Method responsible to perform Scheduling type short code
     * @param message
     * @param numbers
     * @param jobDate
     * @param jobTime
     * @return String
     * @throws Exception
     */
    @Override
    public String sendShortScheduling(String numbers, String message, String jobDate, String jobTime) throws Exception {
        return sendMessage(PLATAFORM_SMS_PATH,
                mountNameValuePairScheduling(PLATAFORM_SMS_PASSWORD, message, numbers, jobDate, jobTime));
    }

    /**
     * Method responsible of get current balance type short code
     * @return String
     * @throws Exception
     */
    @Override
    public String getShortCurrentBalance() throws Exception {
        return sendMessage(PLATAFORM_SMS_PATH,
                mountNameValuePairCurrentBalance(PLATAFORM_SMS_PASSWORD));
    }

    /**
     * Method responsible of get campanha type short code
     * @param idCamapnha
     * @return String
     * @throws Exception
     */
    @Override
    public String getShortCampanha(String idCamapnha) throws Exception {
        return sendMessage(PLATAFORM_SMS_PASSWORD, mountNameValuePairCampanha(PLATAFORM_SMS_PASSWORD, idCamapnha));
    }

    /**
     * Method responsible for sending SMS
     * @param path
     * @param querymsg
     * @return String
     * @throws Exception
     */
    private String sendMessage(final String path, final NameValuePair[] querymsg) throws Exception {
        HttpClient client = new HttpClient();
        HttpMethod method = null;

        try {
            method = getMethod(path, querymsg);

            int httpStatus = client.executeMethod(method);
            return method.getResponseBodyAsString();

        } finally {
            method.releaseConnection();
        }
    }

    /**
     * Method responsible for creating to set a path to set query string in UTF-8 encode
     * @param path
     * @param query
     * @return HttpMethod
     * @throws URIException
     */
    private HttpMethod getMethod(final String path, final NameValuePair[] query) throws URIException {
        HttpMethod method = new GetMethod();

        method.setURI(new URI(HTTP.concat(HOST), true));
        method.setPath(path);

        String queryStr = org.apache.commons.httpclient.util.EncodingUtil.formUrlEncode(query, "UTF-8");
        method.setQueryString(queryStr);

        return method;
    }

    /**
     * Method responsible mount NameValuePair[] Send Simple
     * @param inputPassword
     * @param inputMessage
     * @param inputNumbers
     * @return NameValuePair[]
     */
    private NameValuePair[] mountNameValuePairSendSimple(final String inputPassword,
                                                         final String inputMessage, final String inputNumbers) {

        NameValuePair password = new NameValuePair(DESC_PASSWORD, inputPassword);
        NameValuePair msg = new NameValuePair(DESC_MESSAGE, inputMessage);
        NameValuePair numbers = new NameValuePair(DESC_NUMBERS, inputNumbers);

        return new NameValuePair[] { actionSendMessage, login, password, msg, numbers};
    }

    /**
     * Method responsible mount NameValuePair[] CallBack
     * @param inputPassword
     * @param inputMessage
     * @param inputNumbers
     * @param inputCallBack
     * @return NameValuePair[]
     */
    private NameValuePair[] mountNameValuePairCallBack(final String inputPassword, final String inputMessage,
                                                       final String inputNumbers, final String inputCallBack) {

        NameValuePair password = new NameValuePair(DESC_PASSWORD, inputPassword);
        NameValuePair msg = new NameValuePair(DESC_MESSAGE, inputMessage);
        NameValuePair numbers = new NameValuePair(DESC_NUMBERS, inputNumbers);
        NameValuePair callBack = new NameValuePair(DESC_URL_CALLBACK, inputCallBack);

        return new NameValuePair[] { actionSendMessage, login, password, msg, numbers, callBack};
    }

    /**
     * Method responsible mount NameValuePair[] current balance
     * @param inputPassword
     * @param inputMessage
     * @param inputNumbers
     * @param inputJobDate
     * @param inputJobTime
     * @return
     */
    private NameValuePair[] mountNameValuePairScheduling(final String inputPassword,
                                                         final String inputMessage, final String inputNumbers,
                                                         final String inputJobDate, final String inputJobTime) {

        NameValuePair password = new NameValuePair(DESC_PASSWORD, inputPassword);
        NameValuePair msg = new NameValuePair(DESC_MESSAGE, inputMessage);
        NameValuePair numbers = new NameValuePair(DESC_NUMBERS, inputNumbers);
        NameValuePair jobDate = new NameValuePair(DESC_JOB_DATE, inputJobDate); //exe: 30/08/2016
        NameValuePair jobTime = new NameValuePair(DESC_JOB_TIME, inputJobTime);  //exe: 14:00

        return new NameValuePair[] { actionSendMessage, login, password, msg, numbers, jobDate, jobTime};
    }

    /**
     * Method responsible mount NameValuePair[] current balance
     * @param inputPassword
     * @return NameValuePair[]
     */
    private NameValuePair[] mountNameValuePairCurrentBalance(final String inputPassword) {
        NameValuePair password = new NameValuePair(DESC_PASSWORD, inputPassword);

        return new NameValuePair[] { actionGetBalance, login, password };
    }

    /**
     * Method responsible mount NameValuePair[] of campanha
     * @param inputPassword
     * @param inputCampanha
     * @return
     */
    private NameValuePair[] mountNameValuePairCampanha(final String inputPassword, final String inputCampanha) {

        NameValuePair password = new NameValuePair(DESC_PASSWORD, inputPassword);
        NameValuePair campanha = new NameValuePair(DESC_ID_CAMPANHA, inputCampanha);

        return new NameValuePair[] { actionGetStatus, login, password, campanha};
    }
}
