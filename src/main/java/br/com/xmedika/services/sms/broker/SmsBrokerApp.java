package br.com.xmedika.services.sms.broker;

import br.com.xmedika.services.sms.broker.config.BaseConfig;
import br.com.xmedika.services.sms.broker.config.DataSourceConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.web.context.request.RequestContextListener;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import java.util.Locale;
import java.util.TimeZone;

@Import({
		BaseConfig.class,
		DataSourceConfig.class,
		//ResourceServerConfiguration.class,
		//GlobalMethodSecurityConfiguration.class,
		//JwtConfiguration.class
		//WebConfig.class,
})
@SpringBootApplication/*(exclude = {SecurityAutoConfiguration.class })*/
public class SmsBrokerApp extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(SmsBrokerApp.class, args);
	}

	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {

		servletContext.addListener(new RequestContextListener());
		super.onStartup(servletContext);

		Locale.setDefault(new Locale("pt", "BR"));
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
	}

	@Bean
	public JavaMailSenderImpl mailSender() {
		JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();

		javaMailSender.setProtocol("SMTP");
		javaMailSender.setHost("127.0.0.1");
		javaMailSender.setPort(25);

		return javaMailSender;
	}
}
