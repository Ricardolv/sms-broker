package br.com.xmedika.services.sms.broker.service.impl;

import br.com.socialpro.component.MessageUtil;
import br.com.socialpro.service.impl.AbstractCrudService;
import br.com.xmedika.services.sms.broker.model.Account;
import br.com.xmedika.services.sms.broker.model.Message;
import br.com.xmedika.services.sms.broker.model.QMessage;
import br.com.xmedika.services.sms.broker.model.dto.MessageDTO;
import br.com.xmedika.services.sms.broker.repository.MessageRepository;
import br.com.xmedika.services.sms.broker.service.AccountService;
import br.com.xmedika.services.sms.broker.service.MessageService;
import br.com.xmedika.services.sms.broker.util.DateTimeUtil;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.jpa.impl.JPAQuery;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.Hours;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

import static br.com.xmedika.services.sms.broker.model.SmsConstants.DEBIT_A_CREDIT;
import static br.com.xmedika.services.sms.broker.model.SmsConstants.ENDPOINT_IMED_CUSTOMER_ALERT;
import static br.com.xmedika.services.sms.broker.model.SmsConstants.ENDPOINT_IMED_CUSTOMER_RETURN;

@Service
public class MessageServiceImpl extends AbstractCrudService<Message, Long, MessageRepository> implements MessageService {

    private static Logger logger = LoggerFactory.getLogger(MessageServiceImpl.class);

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private AccountService accountService;

    @Autowired
    DateTimeUtil dateTimeUtil;

    @Autowired
    MessageUtil messageUtil;

    @Override
    protected MessageRepository getRepository() {
        return messageRepository;
    }

    @Override
    public EntityPathBase<Message> entityPath() {
        return QMessage.message;
    }

    /**
     * Metodo responsavel por buscar uma messagem por id da campanha e pelo celular
     * @param data
     * @param numberMobile
     * @return Message
     */
    @Override
    public Message findMessageByDataAndByNumberMobile(String data, String numberMobile) {

        JPAQuery<Message> query = createQuery();
        query.where(QMessage.message.data.eq(data)
                .and(QMessage.message.mobileNumber.eq(numberMobile))
        );
        return query.fetchFirst();
    }

    @Override
    public List<Message> findAllByMessageIsGenerateAlertPhone(Long accountId) {
        JPAQuery<Message> query = createQuery();
        query.where(QMessage.message.isGenerateAlertPhone.isTrue()
                .and(QMessage.message.isReferenceToSendAlert.isFalse())
                .and(QMessage.message.idAccountClient.eq(accountId))
        );
        return query.fetch();
    }

    @Override
    public boolean thereIsNotAccount(Long idDomain, Long idAccount) {
        return accountService.thereIsNotAccount(idDomain, idAccount);
    }

    @Override
    public void alertNotResponseQuery() {

        Long pageSize = 10L;
        Long count = accountService.count();
        Long pages = (count + pageSize - 1) / pageSize;
        Long page = 0L;

        try {
            while (pages > page) {
                Page<Account> listAccounts = accountService.findAll(getPageable(pageSize, page));
                listAccounts.forEach(account -> {
                    List<Message> messageList = findAllByMessageIsGenerateAlertPhone(account.getId());
                    if (null != messageList && !messageList.isEmpty()) {

                        messageList.forEach( message ->
                                processAlert(message)
                        );
                    }
                });
                page++;
            }
        } catch (Exception e) {
            logger.error("#############################ERROR PROCESSING GENERATOR OF ALERT SMS #############################");
            throw e;
        }
    }

    /**
     *
     * @param message
     */
    private void processAlert(Message message) {
        if (null != message && null != message.getHoursGenerateAlertPhone()) {

            int hours = Hours.hoursBetween(DateTime.now(), message.getStartEvent()).getHours();

            if (message.getHoursGenerateAlertPhone().intValue() >= hours ) {

                message.setIsReferenceToSendAlert(true);

                Account account = accountService.lowInTheBalance(message.getIdAccountClient(), DEBIT_A_CREDIT);
                accountService.save(account);

                message.setDebit(message.getDebit() + DEBIT_A_CREDIT);
                messageRepository.save(message);

                try {
                    MessageDTO messageDTO = getMessageDtoInstance(message);
                    HttpEntity<Object> request = new HttpEntity<Object>(messageDTO, getHeaders());
                    new RestTemplate().postForObject(ENDPOINT_IMED_CUSTOMER_ALERT, request, MessageDTO.class);
                } catch (Exception e) {
                    logger.error(messageUtil.getMessage("error.sms.alert"));
                }
            }
        }
    }

    /* BUSINESS RULE */

    /**
     * Metodo responsavel por converter objeto MessageDTO para uma instancia do objeto Message
     * @param messageDTO
     * @return Message
     */
    @Override
    public Message convertMessageDTOtoMessage(MessageDTO messageDTO) {
        Message message = new Message();
        message.setCreatedTime(DateTime.now());

        message.setStatus(messageDTO.getStatus());
        message.setData(messageDTO.getData());
        message.setMsg(messageDTO.getMsg());

        return message;
    }

    /**
     * Metodo responsavel criar uma instancia de messagem com base de dados do cliente
     * @param mobileNumber
     * @param text
     * @param jobDateTime
     * @return Message
     */
    @Override
    public Message getMessageInstance(String mobileNumber, String text, DateTime jobDateTime) {
        Message message = new Message();
        message.setCreatedTime(DateTime.now());
        message.setMobileNumber(mobileNumber);
        message.setBody(text);

        if (null != jobDateTime) {
            message.setJobDateTime(jobDateTime);
        }

        return message;
    }

    /**
     * Metodo responsavel criar uma instancia de messagem com base de dados do cliente
     * @param messageDto
     * @return Message
     */
    @Override
    public Message getMessageInstance(MessageDTO messageDto) {

        Message message = new Message();
        message.setCreatedTime(DateTime.now());
        message.setStartEvent(messageDto.getStartEvent());
        message.setMobileNumber(messageDto.getMobileNumber());
        message.setBody(messageDto.getText());
        message.setIdDomain(messageDto.getIdDomain());
        message.setIdEvent(messageDto.getIdEvent());
        message.setIdAccountClient(messageDto.getIdAccountClient());
        message.setIsGenerateAlertPhone(messageDto.getIsGenerateAlertPhone());
        message.setHoursGenerateAlertPhone(messageDto.getHoursGenerateAlertPhone());
        message.setSendStart(messageDto.getSendStart());
        message.setSendEnd(messageDto.getSendEnd());

        return message;
    }

    /**
     * Metodo responsavel criar uma instancia de messagem com base de dados do cliente
     * @param message
     * @return Message
     */
    @Override
    public MessageDTO getMessageDtoInstance(Message message) {

        MessageDTO messageDto = new MessageDTO();
        messageDto.setStartEvent(message.getStartEvent());
        messageDto.setMobileNumber(message.getMobileNumber());
        messageDto.setText(message.getBody());
        messageDto.setIdDomain(message.getIdDomain());
        messageDto.setIdEvent(message.getIdEvent());
        messageDto.setIdAccountClient(message.getIdAccountClient());
        messageDto.setIsGenerateAlertPhone(message.getIsGenerateAlertPhone());
        messageDto.setHoursGenerateAlertPhone(message.getHoursGenerateAlertPhone());
        messageDto.setCustomerResponse(message.getCustomerResponse());
        messageDto.setSendStart(message.getSendStart());
        messageDto.setSendEnd(message.getSendEnd());

        return messageDto;
    }

    /**
     * Metodo responsavel por criar uma instancia de Message com base de dados do callback.
     *
     * Apos uma mensagem de confirmacao ser enviada recebe dois retorno pela API do fornecedor.
     *
     * - Primeiro retorno, confirmacao do disparo da mensagem para o cliente com os respectivos campos :
     *  Exe:
     *      data (id da campanha) - celular - resposta (Vazio) - data_resposta (data do envio) - status (SUCESSO)
     *
     * - Segundo retorno, resposta do cliente referente a mensagem de confirmacao com os respectivos campos :
     *  Exe:
     *      data (id da campanha), celular, resposta (resposta do cliente), data_resposta (data do disparo), status (RECEBIDO SUCESSO)
     *
     * @param dataCallback
     * @return Message
     */
    @Override
    public Message returnCallback(Object[] dataCallback) {
        Message message = null;

        int data = null != dataCallback[0] ? (Integer) dataCallback[0] : 0;
        String celular = null != dataCallback[1] ? (String) dataCallback[1] : "";
        String resposta = null != dataCallback[2] ? (String) dataCallback[2] : "";
        String dataResposta = null != dataCallback[3] ?  (String) dataCallback[3] : "";
        String status = null != dataCallback[4] ? (String) dataCallback[4] : "";
        String internoKey = null !=  dataCallback[5] ? (String) dataCallback[5] : "";


        if (data > 0 && StringUtils.isNotEmpty(celular)) {

            message = findMessageByDataAndByNumberMobile(String.valueOf(data), celular);

            if (StringUtils.isNotBlank(status)) {
                message.setMsg(status);
            }

            /**
             * A primeira condicao e referente ao envio da mensagem para o destinatario (Cliente),
             * pois a API sempre envia um CALLBACK informando a data e hora do envio da mensagem.
             *
             * A segunda condicao ja e a resposta do destinatario (Cliente),
             * debitando o valor de um credito e enviando a resposta do cliente para o imed.
             */
            if (null == message.getSendStart()) {
                message.setSendStart(new DateTime(dateTimeUtil.convertStringToDateFormatYMDHMS(dataResposta)));

            } else {

                message.setCustomerResponse(resposta);
                message.setSendEnd(new DateTime(dateTimeUtil.convertStringToDateFormatYMDHMS(dataResposta)));

                Account account = accountService.lowInTheBalance(message.getIdAccountClient(), DEBIT_A_CREDIT);
                accountService.save(account);

                message.setDebit(message.getDebit() + DEBIT_A_CREDIT);

                try {
                    MessageDTO messageDTO = getMessageDtoInstance(message);

                    HttpEntity<Object> request = new HttpEntity<Object>(messageDTO, getHeaders());
                    new RestTemplate().postForObject(ENDPOINT_IMED_CUSTOMER_RETURN, request, MessageDTO.class);

                } catch (Exception e) {
                    logger.error(messageUtil.getMessage("error.sms.return"));
                }

            }
        }

        return message;
    }

    private static HttpHeaders getHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }

    private Pageable getPageable(Long pageSize, Long page) {
        return new PageRequest(pageSize.intValue() * page.intValue(), pageSize.intValue());
    }

}
