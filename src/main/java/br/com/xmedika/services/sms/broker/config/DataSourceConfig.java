package br.com.xmedika.services.sms.broker.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@Configuration
@EnableAutoConfiguration
@EnableWebSecurity
@EnableJpaRepositories(basePackages = {"br.com.xmedika.services.sms.broker.repository"}, transactionManagerRef = "spTransactionManager")
@EntityScan(basePackages = {"br.com.xmedika.services.sms.broker.model"})
public class DataSourceConfig extends br.com.socialpro.config.DataSourceConfig {

}
