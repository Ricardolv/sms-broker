package br.com.xmedika.services.sms.broker.service.impl;

import br.com.socialpro.service.impl.AbstractCrudService;
import br.com.xmedika.services.sms.broker.model.Account;
import br.com.xmedika.services.sms.broker.model.Domain;
import br.com.xmedika.services.sms.broker.model.QAccount;
import br.com.xmedika.services.sms.broker.repository.AccountRepository;
import br.com.xmedika.services.sms.broker.service.AccountService;
import br.com.xmedika.services.sms.broker.service.DomainService;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.jpa.impl.JPAQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountServiceImpl extends AbstractCrudService<Account, Long, AccountRepository> implements AccountService {

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    private DomainService domainService;

    @Override
    protected AccountRepository getRepository() {
        return accountRepository;
    }

    @Override
    public EntityPathBase<Account> entityPath() {
        return QAccount.account;
    }

    /**
     * Metodo responsavel por dar baixa de credito
     * @param accountId
     * @return Account
     */
    @Override
    public Account lowInTheBalance(Long accountId, Integer credit) {
        Account account = findOneByAccountClient(accountId);
        account.setAmountOfCredit(account.getAmountOfCredit() - credit);
        return account;
    }

    /**
     * Metodo responsavel por verificar limite de credito
     * @param value
     * @return boolean
     */
    @Override
    public boolean noCreditLimit(Integer value) {
        return value == 0;
    }

    /**
     *  Metodo responsavel por add credito
     * @param accountId
     * @param value
     * @return Account
     */
    @Override
    public Account addCredit(Long accountId, Integer value) {
        Account account = findOneByAccountClient(accountId);
        account.setAmountOfCredit(account.getAmountOfCredit() + value);
        account = accountRepository.save(account);
        return account;
    }

    /**
     *  Metodo responsavel por add credito
     * @param accountId
     * @param value
     * @return Account
     */
    @Override
    public Account addCredit(Long domainId, Long accountId, Integer value) {
        Account account = findOneByDomainAndByAccountClient(domainId, accountId);
        account.setAmountOfCredit(account.getAmountOfCredit() + value);
        account = accountRepository.save(account);
        return account;
    }

    @Override
    public Account findOneByAccountClient(Long id) {
        JPAQuery<Account> query = createQuery();
        query.where(QAccount.account.accountClientId.eq(id));
        return query.fetchFirst();
    }

    @Override
    public Account findOneByDomainAndByAccountClient(Long idDomain, Long id) {
        JPAQuery<Account> query = createQuery();
        query.where(QAccount.account.domain.id.eq(idDomain)
                .and(QAccount.account.accountClientId.eq(id)));
        return query.fetchFirst();
    }

    @Override
    public List<Account> listAccountByDomain(Long idDomain) {
        JPAQuery<Account> query = createQuery();
        query.where(QAccount.account.domain.id.eq(idDomain));
        return query.fetch();
    }

    @Override
    public boolean thereIsNotAccount(Long idDomain, Long idAccount) {
        return !thereIsAccount(idDomain, idAccount);
    }

    @Override
    public boolean thereIsAccount(Long idDomain, Long idAccount) {
        return  null != findOneByDomainAndByAccountClient(idDomain, idAccount);
    }

    @Override
    public Domain getDomain(Long domainId) {
        return domainService.findOne(domainId);
    }
}
