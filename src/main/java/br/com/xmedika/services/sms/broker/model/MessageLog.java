package br.com.xmedika.services.sms.broker.model;


import lombok.Data;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "message_log")
public class MessageLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "message_id", nullable = false)
    private Long id;

    @Column(name = "created_time", nullable = false)
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime", parameters = {
            @org.hibernate.annotations.Parameter(name = "databaseZone", value = "UTC"),
            @org.hibernate.annotations.Parameter(name = "javaZone", value = "UTC")})
    private DateTime createdTime;

    @Column(name = "event_start", nullable = false)
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime", parameters = {
            @org.hibernate.annotations.Parameter(name = "databaseZone", value = "UTC"),
            @org.hibernate.annotations.Parameter(name = "javaZone", value = "UTC")})
    private DateTime startEvent;

    @Column(name = "status")
    private Integer status;

    //id da campanha
    @Column(name = "data", length = 50)
    private String data;

    @Column(name = "msg", length = 30)
    private String msg;

    @Column(name = "mobile_number", length = 15)
    private String mobileNumber;

    @Column(name = "body", length = 156)
    private String body;

    @Column(name = "job_date_time")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime", parameters = {
            @org.hibernate.annotations.Parameter(name = "databaseZone", value = "UTC"),
            @org.hibernate.annotations.Parameter(name = "javaZone", value = "UTC")})
    private DateTime jobDateTime;

    @Column(name = "type_send")
    @Type(type = "TypeSend")
    private TypeSendEnum typeSend;

    @Column(name = "type_send_message")
    @Type(type = "typeSendMessage")
    private SendMessageEnum typeSendMessage;

    @Column(name = "send_start")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime", parameters = {
            @org.hibernate.annotations.Parameter(name = "databaseZone", value = "UTC"),
            @org.hibernate.annotations.Parameter(name = "javaZone", value = "UTC")})
    private DateTime sendStart;


    @Column(name = "send_end")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime", parameters = {
            @org.hibernate.annotations.Parameter(name = "databaseZone", value = "UTC"),
            @org.hibernate.annotations.Parameter(name = "javaZone", value = "UTC")})
    private DateTime sendEnd;

    @Column(name = "customer_response", length = 30)
    private String customerResponse;

    @Column(name = "limit_send")
    private Integer limitSend = 0;

    @Column(name = "message_error")
    private boolean isMessageError = false;

    @Column(name = "id_domain", nullable = false)
    private Long idDomain;

    @Column(name = "id_account", nullable = false)
    private Long idAccountClient;

    @Column(name = "id_event")
    private Long idEvent;

    //opcao - gerar um alerta para confirmacao por telefone se o paciente não responder o SMS
    @Column(name = "generate_alert_phone", nullable = false)
    private Boolean isGenerateAlertPhone = false;

    //gerar um alerta para confirmacao por telefone se o paciente nao responder o SMS - horas
    @Column(name = "hours_generate_alert_phone")
    private Short hoursGenerateAlertPhone;

    //referencia que a mensagem de alerta ja foi enviada
    @Column(name = "reference_send_alert")
    private Boolean isReferenceToSendAlert;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MessageLog message = (MessageLog) o;

        return id != null ? id.equals(message.id) : message.id == null;

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
