package br.com.xmedika.services.sms.broker.model;

import br.com.socialpro.orm.PersistentEnum;
import br.com.xmedika.services.sms.broker.service.Sms;
import br.com.xmedika.services.sms.broker.service.sms.*;

public enum SendMessageEnum implements PersistentEnum {

    SEND_LONG_SIMPLE((short) 0, "SendLongSimple") {
        @Override
        public Sms sendMessage(String[] data) {
            //0: numbers - 1: message
            return new SendLongSimple(data[0], data[1]);
        }
    },

    SEND_LONG_CALL_BACK((short) 1, "SendLongCallBack")  {
        @Override
        public Sms sendMessage(String[] data) {
            //0: numbers - 1: message
            return new SendLongCallBack(data[0], data[1]);
        }
    },

    SEND_SHORT_SIMPLE((short) 2, "SendShortSimple") {
        @Override
        public Sms sendMessage(String[] data) {
            //0: numbers - 1: message
            return new SendShortSimple(data[0], data[1]);
        }
    },

    SEND_SHORT_CALL_BACK((short) 3, "SendShortCallBack") {
        @Override
        public Sms sendMessage(String[] data) {
            //0: numbers - 1: message
            return new SendShortCallBack(data[0], data[1]);
        }
    },

    GET_LONG_CURRENT_BALANCE((short) 4, "GetLongCurrentBalance") {
        @Override
        public Sms sendMessage(String[] data) {
            return new GetLongCurrentBalance();
        }
    },

    GET_SHORT_CURRENT_BALANCE((short) 5, "GetShortCurrentBalance") {
        @Override
        public Sms sendMessage(String[] data) {
            return new GetShortCurrentBalance();
        }
    },

    SEND_LONG_SCHEDULING((short) 6, "SendLongSchedulingApi") {
        @Override
        public Sms sendMessage(String[] data) {
            //0: numbers - 1: message - 2 jobDate - 3 jobTime
            return new SendLongSchedulingApi(data[0], data[1], data[2], data[3]);
        }
    },

    SEND_SHORT_SCHEDULING((short) 7, "SendShortSchedulingApi") {
        @Override
        public Sms sendMessage(String[] data) {
            //0: numbers - 1: message
            return new SendShortScheduling(data[0], data[1]);
        }
    },

    SEND_LONG_SCHEDULING_API((short) 8, "SendLongSchedulingApi") {
        @Override
        public Sms sendMessage(String[] data) {
            //0: numbers - 1: message - 2 jobDate - 3 jobTime
            return new SendLongSchedulingApi(data[0], data[1], data[2], data[3]);
        }
    },

    SEND_SHORT_SCHEDULING_API((short) 9, "SendShortSchedulingApi") {
        @Override
        public Sms sendMessage(String[] data) {
            //0: numbers - 1: message - 2 jobDate - 3 jobTime
            return new SendShortSchedulingApi(data[0], data[1], data[2], data[3]);
        }
    };

    private Short code;
    private String text;

    SendMessageEnum(Short code, String text) {
        this.code = code;
        this.text = text;
    }

    public abstract Sms sendMessage(String[] data);

    public Short getCode() {
        return code;
    }

    public void setCode(Short code) {
        this.code = code;
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return this.getText();
    }

    @Override
    public Short getPersistentValue() {
        return code.shortValue();
    }

    @Override
    public PersistentEnum fromPersistentValue(Short value) {
        for (SendMessageEnum type : SendMessageEnum.values()) {
            if (type.code.equals(value)) {
                return type;
            }
        }
        return null;
    }

    public static SendMessageEnum convertStringToEnum (String value) {

        for (SendMessageEnum sendMessageEnum: SendMessageEnum.values() ) {
            if (sendMessageEnum.getText().equalsIgnoreCase(value)) {
                return sendMessageEnum;
            }
        }

        return null;
    }

    public static SendMessageEnum convertIntegerToEnum (int value) {

        for (SendMessageEnum sendMessageEnum: SendMessageEnum.values() ) {
            if (sendMessageEnum.getCode().intValue() == value) {
                return sendMessageEnum;
            }
        }

        return null;
    }
}
