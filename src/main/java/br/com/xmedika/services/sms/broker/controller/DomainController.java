package br.com.xmedika.services.sms.broker.controller;

import br.com.socialpro.controller.AbstractCrudController;
import br.com.xmedika.services.sms.broker.model.Account;
import br.com.xmedika.services.sms.broker.model.Domain;
import br.com.xmedika.services.sms.broker.service.DomainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping({"/domain"})
public class DomainController extends AbstractCrudController<Domain, Long, DomainService> {

    @Autowired
    private DomainService domainService;

    @RequestMapping(method = {RequestMethod.GET}, value = "/credits/{idDomain}/{idAccountClient}")
    @ResponseBody
    //@PreAuthorize("hasAnyRole(this.readRoles)")
    public String getCredits(@PathVariable("idDomain") Long idDomain, @PathVariable("idAccountClient") Long idAccountClient) {
        Account account = domainService.getAccount(idDomain, idAccountClient);
        return account != null ? account.getAmountOfCredit().toString() : null;
    }

    @RequestMapping(method = {RequestMethod.POST}, value = "/credit/{idDomain}/{idAccountClient}/{value}")
    @ResponseBody
    //@PreAuthorize("hasAnyRole(this.readRoles)")
    public String addCredit(@PathVariable("idDomain") Long idDomain,
                            @PathVariable("idAccountClient") Long idAccountClient,
                            @PathVariable("value") Integer value) {

        Account account = domainService.getAccount(idDomain, idAccountClient);
        return account != null ?
                domainService.addCreditAccount(idDomain, idAccountClient, value).getAmountOfCredit().toString() : null ;
    }

    @RequestMapping(method = {RequestMethod.GET}, value = "/list")
    @ResponseBody
    //@PreAuthorize("hasAnyRole(this.readRoles)")
    public List<Domain> listAll() {
        return this.getService().findAll();
    }

    @Override
    @RequestMapping(method = {RequestMethod.PUT})
    @ResponseBody
    //@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public Domain update(@RequestBody @Valid Domain entity) {
        return this.getService().save(entity);
    }

    @Override
    @RequestMapping(
            value = {"/{id}"},
            method = {RequestMethod.DELETE}
    )
    //@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public Boolean delete(@PathVariable("id") Long id) {
        this.getService().delete(id);
        return true;
    }

    @Override
    public DomainService getService() {
        return this.domainService;
    }

    @Override
    public String[] getEditRoles() {
        return new String[]{"ROLE_ADMIN"};
    }

    @Override
    public String[] getReadRoles() {
        return new String[]{"ROLE_ADMIN"};
    }
}
