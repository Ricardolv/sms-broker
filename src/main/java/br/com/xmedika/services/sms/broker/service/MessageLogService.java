package br.com.xmedika.services.sms.broker.service;


import br.com.socialpro.service.CrudService;
import br.com.xmedika.services.sms.broker.model.TypeSendEnum;
import br.com.xmedika.services.sms.broker.model.Message;
import br.com.xmedika.services.sms.broker.model.MessageLog;
import br.com.xmedika.services.sms.broker.model.SendMessageEnum;

import java.util.List;

public interface MessageLogService extends CrudService<MessageLog, Long> {

    List<MessageLog> messageLogNotTypeErrorNotLimitThree();

    void createMessageError(Exception e, SendMessageEnum sendMessageEnum);
    void createMessageLog(Message entity, TypeSendEnum typeSendEnum, SendMessageEnum sendMessageEnum);
}
