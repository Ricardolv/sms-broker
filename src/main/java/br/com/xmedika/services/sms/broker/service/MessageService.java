package br.com.xmedika.services.sms.broker.service;


import br.com.socialpro.service.CrudService;
import br.com.xmedika.services.sms.broker.model.Account;
import br.com.xmedika.services.sms.broker.model.dto.MessageDTO;
import br.com.xmedika.services.sms.broker.model.Message;
import org.joda.time.DateTime;

import java.util.List;

public interface MessageService extends CrudService<Message, Long> {

    Message convertMessageDTOtoMessage(MessageDTO messageDTO);

    Message returnCallback(Object[] dataCallback);

    Message getMessageInstance(String mobileNumber, String text, DateTime jobDateTime);

    Message getMessageInstance(MessageDTO messageDTO);

    MessageDTO getMessageDtoInstance(Message message);

    Message findMessageByDataAndByNumberMobile(String data, String numberMobile);

    boolean thereIsNotAccount(Long idDomain, Long idAccount);

    void alertNotResponseQuery();

    List<Message> findAllByMessageIsGenerateAlertPhone(Long accountId);
}
