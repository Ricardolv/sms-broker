package br.com.xmedika.services.sms.broker.config;


import br.com.socialpro.AppManifest;
import br.com.socialpro.config.AbstractBaseConfig;
import br.com.xmedika.services.sms.broker.Manifest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"br.com.xmedika.services.sms.broker.service", "br.com.xmedika.services.sms.broker.tasklet", "br.com.xmedika.services.sms.broker.component"})
public class BaseConfig extends AbstractBaseConfig {

    @Override
    protected String getAlias() {
        return "smsbroker";
    }

    @Bean
    public AppManifest appManifest() {
        return new Manifest();
    }
}
