package br.com.xmedika.services.sms.broker.util;


import org.joda.time.DateTime;

import java.util.Date;

public interface DateTimeUtil {

    String convertToYYYYMMDD(Date date);

    String convertDateTimeToStringFormatDDMMYYYY(DateTime dateTime);
    String convertDateTimeToStringFormatHHmm(DateTime dateTime);
    Date convertStringToDateFormatYMDHMS(String date);
    Date convertStringToDateFormatDMYHMS(String inputDate);
    DateTime convertStringToDateFormatDMYHMS(String date, String time);

}
