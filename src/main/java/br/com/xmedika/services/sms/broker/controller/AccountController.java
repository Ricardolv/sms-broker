package br.com.xmedika.services.sms.broker.controller;

import br.com.socialpro.controller.AbstractCrudController;
import br.com.xmedika.services.sms.broker.model.Account;
import br.com.xmedika.services.sms.broker.model.Domain;
import br.com.xmedika.services.sms.broker.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping({"/account"})
public class AccountController extends AbstractCrudController<Account, Long, AccountService> {

    @Autowired
    private AccountService accountService;

    @RequestMapping(method = {RequestMethod.GET}, value = "/credits/{domainId}/{id}")
    @ResponseBody
    //@PreAuthorize("hasAnyRole(this.readRoles)")
    public String getCredits(@PathVariable("domainId") Long domainId, @PathVariable("id") Long accountId) {
        Account account = accountService.findOneByDomainAndByAccountClient(domainId, accountId);
        return account != null ? account.getAmountOfCredit().toString() : null;
    }

    @RequestMapping(method = {RequestMethod.GET}, value = "/credits/{id}")
    @ResponseBody
    //@PreAuthorize("hasAnyRole(this.readRoles)")
    public String getCredits(@PathVariable("id") Long accountId) {
        Account account = accountService.findOneByAccountClient(accountId);
        return account != null ? account.getAmountOfCredit().toString() : null;
    }

    @RequestMapping(method = {RequestMethod.POST}, value = "/credit/{accountId}/{value}")
    @ResponseBody
    //@PreAuthorize("hasAnyRole(this.readRoles)")
    public String addCredit(@PathVariable("accountId") Long accountId, @PathVariable("value") Integer value) {
        Account account = accountService.addCredit(accountId, value);
        return account != null ? account.getAmountOfCredit().toString() : null;
    }

    @RequestMapping(method = {RequestMethod.POST}, value = "/credit/{domainId}/{accountId}/{value}")
    @ResponseBody
    //@PreAuthorize("hasAnyRole(this.readRoles)")
    public String addCredit(@PathVariable("accountId") Long accountId,
                            @PathVariable("domainId") Long domainId,
                            @PathVariable("value") Integer value) {
        Account account = accountService.addCredit(domainId, accountId, value);
        return account != null ? account.getAmountOfCredit().toString() : null;
    }


    @RequestMapping(method = {RequestMethod.POST}, value = "/create/{domainId}/{accountId}")
    @ResponseBody
    public void create(@PathVariable("domainId") Long domainId, @PathVariable("accountId") Long accountId) {

        Domain domain = accountService.getDomain(domainId);

        Account account = new Account();
        account.setAmountOfCredit(0);
        account.setAccountClientId(accountId);
        account.setDomain(domain);
        accountService.save(account);
    }

    @RequestMapping(method = {RequestMethod.GET}, value = "/list/{domainId}")
    @ResponseBody
    //@PreAuthorize("hasAnyRole(this.readRoles)")
    public List<Account> listAccountByDomain(@PathVariable("domainId") Long domainId) {
        return this.getService().listAccountByDomain(domainId);
    }

    @RequestMapping(method = {RequestMethod.GET}, value = "/list")
    @ResponseBody
    //@PreAuthorize("hasAnyRole(this.readRoles)")
    public List<Account> listAll() {
        return this.getService().findAll();
    }

    @Override
    public AccountService getService() {
        return this.accountService;
    }

    @Override
    public String[] getEditRoles() {
        return new String[]{"ROLE_ADMIN"};
    }

    @Override
    public String[] getReadRoles() {
        return new String[]{"ROLE_ADMIN"};
    }
}
