package br.com.xmedika.services.sms.broker.service;


import br.com.socialpro.service.CrudService;
import br.com.xmedika.services.sms.broker.model.Account;
import br.com.xmedika.services.sms.broker.model.Domain;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DomainService extends CrudService<Domain, Long> {

    Account getAccount(Long idDomain, Long idAccountClient);

    Account addCreditAccount(Long idDomain, Long accountId, Integer value);

}
