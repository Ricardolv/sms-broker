package br.com.xmedika.services.sms.broker.model;

import br.com.socialpro.orm.PersistentEnum;

public enum TypeSendEnum implements PersistentEnum {

    LONG_CODE((short) 1, "LongCode"),
    SHORT_CODE((short) 2, "ShortCode");

    private Short code;
    private String text;

    TypeSendEnum(Short code, String text) {
        this.code = code;
        this.text = text;
    }

    public Short getCode() {
        return code;
    }

    public void setCode(Short code) {
        this.code = code;
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return this.getText();
    }

    @Override
    public Short getPersistentValue() {
        return code.shortValue();
    }

    @Override
    public PersistentEnum fromPersistentValue(Short value) {
        for (TypeSendEnum type : TypeSendEnum.values()) {
            if (type.code.equals(value)) {
                return type;
            }
        }
        return null;
    }

    public static TypeSendEnum convertShortToEnum (short value) {

        for (TypeSendEnum type: TypeSendEnum.values() ) {
            if (type.getCode() == value) {
                return type;
            }
        }
        return null;
    }
}
