package br.com.xmedika.services.sms.broker.repository;

import br.com.xmedika.services.sms.broker.model.Account;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends PagingAndSortingRepository<Account, Long>,
        JpaSpecificationExecutor<Account> {
}
