package br.com.xmedika.services.sms.broker.config.jobs.alert;

import br.com.socialpro.config.BatchConfig;
import br.com.socialpro.transaction.SPTransactionManager;
import br.com.xmedika.services.sms.broker.tasklet.GenerateAlertTasklet;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.interceptor.DefaultTransactionAttribute;

/**
 * Configuração de tarefa que gerar alerta de limite credito automaticamente
 */
@Configuration
@Import({BatchConfig.class})
public class GenerateAlertConfig {

    @Autowired
    private JobBuilderFactory jobs;

    @Autowired
    private StepBuilderFactory steps;

    @Autowired
    private GenerateAlertTasklet generateAlertTasklet;

    @Autowired
    private SPTransactionManager spTransactionManager;

    @Bean
    @Qualifier(value = "generateAlertJob")
    public Job generateAlertJob() {
        return jobs.get("generateAlertJob")
                .start(stepGenerateAlertStep())
                .build();
    }

    @Bean
    public Step stepGenerateAlertStep() {
        DefaultTransactionAttribute transactionAttribute = new DefaultTransactionAttribute();

        transactionAttribute.setIsolationLevel(TransactionDefinition.ISOLATION_DEFAULT);
        transactionAttribute.setPropagationBehavior(TransactionDefinition.PROPAGATION_NOT_SUPPORTED);
        transactionAttribute.setTimeout(30);

        return steps.get("generateAlertJobStep")
                .transactionManager(spTransactionManager)
                .tasklet(generateAlertTasklet)
                .transactionAttribute(transactionAttribute)
                .build();

    }
}
