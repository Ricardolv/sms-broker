package br.com.xmedika.services.sms.broker.repository;

import br.com.xmedika.services.sms.broker.model.Domain;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DomainRepository extends PagingAndSortingRepository<Domain, Long>,
        JpaSpecificationExecutor<Domain> {

}
