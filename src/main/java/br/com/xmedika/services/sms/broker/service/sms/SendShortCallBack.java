package br.com.xmedika.services.sms.broker.service.sms;

import br.com.socialpro.utils.ApplicationContextProvider;
import br.com.xmedika.services.sms.broker.model.TypeSendEnum;
import br.com.xmedika.services.sms.broker.model.dto.MessageDTO;
import br.com.xmedika.services.sms.broker.model.Message;
import br.com.xmedika.services.sms.broker.model.SendMessageEnum;
import br.com.xmedika.services.sms.broker.service.*;
import org.joda.time.DateTime;

import static br.com.xmedika.services.sms.broker.model.SmsConstants.DEBIT_A_CREDIT;
import static br.com.xmedika.services.sms.broker.model.SmsConstants.RETURN_NOK;
import static br.com.xmedika.services.sms.broker.model.SmsConstants.RETURN_OK;

public class SendShortCallBack implements Sms {

    private MessageService messageService;
    private AccountService accountService;
    private SmsMessageService smsMessageService;
    private MessageLogService messageLogService;

    private String message;
    private String numbers;

    public SendShortCallBack() {

    }

    public SendShortCallBack(String numbers, String message) {
        this.messageService = ApplicationContextProvider.getApplicationContext().getBean(MessageService.class);
        this.accountService = ApplicationContextProvider.getApplicationContext().getBean(AccountService.class);
        this.smsMessageService = ApplicationContextProvider.getApplicationContext().getBean(SmsMessageService.class);
        this.messageLogService = ApplicationContextProvider.getApplicationContext().getBean(MessageLogService.class);

        this.numbers = numbers;
        this.message = message;
    }

    @Override
    public String returnSendSms(Message entity) {

        try {
            return processesBusinessRuleData(entity, getSmsMessageService().sendShortCallBack(getNumbers(), getMessage()));

        } catch (Exception e) {
            //Exception: Caso ocorra erro no envio do sms servico smsMessageService.getLongCurrentBalance()
            getMessageLogService().createMessageLog(entity, TypeSendEnum.SHORT_CODE, SendMessageEnum.SEND_SHORT_CALL_BACK);
            return RETURN_NOK;
        }
    }

    private String processesBusinessRuleData(Message entity, String returnSend) {

        try {
            MessageDTO messageDTO = MessageDTO.convertJsonToObject(returnSend);

            return messageDTO != null && messageDTO.getStatus() == 1 ? finishingProcess(entity, messageDTO) : createMessageLog(entity) ;

        } catch (Exception e) {
            //IOException: Caso ocorra erro de conversao, criacao de message ou na criacao de messageLog
            getMessageLogService().createMessageLog(entity, TypeSendEnum.SHORT_CODE, SendMessageEnum.SEND_SHORT_CALL_BACK);
            return RETURN_NOK;
        }
    }

    /**
     *
     * @param entity
     * @param messageDTO
     * @return String
     * @throws Exception
     */
    private String finishingProcess(Message entity, MessageDTO messageDTO) throws Exception {
        entity.setTypeSendMessage(SendMessageEnum.SEND_SHORT_CALL_BACK);
        entity.setSendStart(DateTime.now());
        entity.setStatus(messageDTO.getStatus());
        entity.setData(messageDTO.getData());
        entity.setMsg(messageDTO.getMsg());

        getAccountService().lowInTheBalance(entity.getIdAccountClient(), DEBIT_A_CREDIT);
        entity.setDebit(DEBIT_A_CREDIT);

        getMessageService().save(entity);

        return RETURN_OK;
    }

    /**
     *
     * @param entity
     * @return String
     * @throws Exception
     */
    private String createMessageLog(Message entity) throws Exception {
        getMessageLogService().createMessageLog(entity, TypeSendEnum.SHORT_CODE, SendMessageEnum.SEND_SHORT_CALL_BACK);
        return RETURN_NOK;
    }

    public MessageService getMessageService() {
        return messageService;
    }

    public AccountService getAccountService() {
        return accountService;
    }

    public SmsMessageService getSmsMessageService() {
        return smsMessageService;
    }

    public MessageLogService getMessageLogService() {
        return messageLogService;
    }

    public String getMessage() {
        return message;
    }

    public String getNumbers() {
        return numbers;
    }
}
