package br.com.xmedika.services.sms.broker.model;

import br.com.socialpro.orm.PersistentEnum;

public enum TypeMessageEnum implements PersistentEnum {

    SMS_TEXT((short) 1, "SMS:TEXT"),


    ERROR((short) 999, "ERROR");

    private Short code;
    private String text;

    TypeMessageEnum(Short code, String text) {
        this.code = code;
        this.text = text;
    }

    public Short getCode() {
        return code;
    }

    public void setCode(Short code) {
        this.code = code;
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return this.getText();
    }

    @Override
    public Short getPersistentValue() {
        return this.code.shortValue();
    }

    @Override
    public PersistentEnum fromPersistentValue(Short value) {
        for (TypeMessageEnum type : TypeMessageEnum.values()) {
            if (type.code.equals(value)) {
                return type;
            }
        }
        return null;
    }

    public static TypeMessageEnum convertShortToEnum (short value) {

        for (TypeMessageEnum typeMessageEnum: TypeMessageEnum.values() ) {
            if (typeMessageEnum.getCode() == value) {
                return typeMessageEnum;
            }
        }

        return null;
    }

    public static TypeMessageEnum convertStringToEnum (String value) {

        for (TypeMessageEnum typeMessageEnum: TypeMessageEnum.values() ) {
            if (typeMessageEnum.getText().equalsIgnoreCase(value)) {
                return typeMessageEnum;
            }
        }

        return null;
    }

}
