package br.com.xmedika.services.sms.broker.controller;


import br.com.socialpro.controller.AbstractCrudController;
import br.com.xmedika.services.sms.broker.model.TypeSendEnum;
import br.com.xmedika.services.sms.broker.model.dto.MessageDTO;
import br.com.xmedika.services.sms.broker.model.Message;
import br.com.xmedika.services.sms.broker.service.MessageService;
import br.com.xmedika.services.sms.broker.service.Sms;
import br.com.xmedika.services.sms.broker.util.DateTimeUtil;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static br.com.xmedika.services.sms.broker.model.SendMessageEnum.*;
import static br.com.xmedika.services.sms.broker.model.SmsConstants.RETURN_NOK;

@RestController
@RequestMapping({"/message"})
public class MessageController extends AbstractCrudController<Message, Long, MessageService> {

    @Autowired
    private MessageService messageService;

    @Autowired
    private DateTimeUtil dateTimeUtil;


    /**
     *
     * @param messageDto
     * @return String
     */
    @RequestMapping(method = RequestMethod.POST, value = "/longsimple")
    @ResponseBody
    public String sendLongSimple(@RequestBody MessageDTO messageDto) {

        if (messageService.thereIsNotAccount(messageDto.getIdDomain(), messageDto.getIdAccountClient())) {
            return RETURN_NOK;
        }

        Message entity = messageService.getMessageInstance(messageDto);
        entity.setTypeSend(TypeSendEnum.LONG_CODE);

        String[] data = { entity.getMobileNumber(), entity.getBody() };
        Sms sendLongSimple = SEND_LONG_SIMPLE.sendMessage(data);

        return sendLongSimple.returnSendSms(entity);
    }

    /**
     *
     * @param mobileNumber
     * @param text
     * @param idDomain
     * @param idAccount
     * @return String
     */
    @RequestMapping(method = RequestMethod.POST, value = "/longsimple/{mobileNumber}/{text}/{idDomain}/{idEvent}/{idAccount}")
    @ResponseBody
    public String sendLongSimple(@PathVariable("mobileNumber") String mobileNumber,
                                 @PathVariable("text") String text,
                                 @PathVariable("idDomain") Long idDomain,
                                 @PathVariable("idEvent") Long idEvent,
                                 @PathVariable("idAccount") Long idAccount) {

        if (messageService.thereIsNotAccount(idDomain, idAccount)) {
            return RETURN_NOK;
        }

        Message entity = messageService.getMessageInstance(mobileNumber, text, null);
        entity.setIdDomain(idDomain);
        entity.setIdAccountClient(idAccount);
        entity.setIdEvent(idEvent);
        entity.setTypeSend(TypeSendEnum.LONG_CODE);

        String[] data = { entity.getMobileNumber(), entity.getBody() };
        Sms sendLongSimple = SEND_LONG_SIMPLE.sendMessage(data);

        return sendLongSimple.returnSendSms(entity);
    }

    /**
     *
     * @param mobileNumber
     * @param text
     * @param idDomain
     * @param idAccount
     * @return String
     */
    @RequestMapping(method = RequestMethod.POST, value = "/longcallback/{mobileNumber}/{text}/{idDomain}/{idEvent}/{idAccount}")
    @ResponseBody
    public String sendLongCallBack(@PathVariable("mobileNumber") String mobileNumber,
                                   @PathVariable("text") String text,
                                   @PathVariable("idDomain") Long idDomain,
                                   @PathVariable("idEvent") Long idEvent,
                                   @PathVariable("idAccount") Long idAccount) {

        if (messageService.thereIsNotAccount(idDomain, idAccount)) {
            return RETURN_NOK;
        }

        Message entity = messageService.getMessageInstance(mobileNumber, text, null);
        entity.setIdDomain(idDomain);
        entity.setIdAccountClient(idAccount);
        entity.setIdEvent(idEvent);
        entity.setTypeSend(TypeSendEnum.LONG_CODE);

        String[] data = { entity.getMobileNumber(), entity.getBody() };
        Sms sendLongCallBack = SEND_LONG_CALL_BACK.sendMessage(data);

        return sendLongCallBack.returnSendSms(entity);
    }

    /**
     Parâmetros contidos no data_callback:
     codigo_campanha; celular; resposta; data_resposta; status; interno_key

     [0] codigo_campanha = Código da campanha retornado pela função sendsms (Tipo de dados Inteiro)
     [1] celular = Número do celular de retonro (Tipo de dados String)
     [2] resposta = Mensagem SMS retornada (Tipo de Dados String)
     [3] data_resposta = Data de recebimento da mensagem (Tipo de Dados String - Formato: yyyyMM-dd HH:mm:ss)
     [4] status = Status da entrega (Tipo de Dados String)
     [5] interno_key = Código de referência passado pela função sendsms (Tipo de Dados String)

     * @param dataCallback
     * @return String
     */
    @RequestMapping(method = RequestMethod.POST, value = "/longcallback/{data_callback}")
    @ResponseBody
    public void returnLongCallBack(@PathVariable("data_callback") Object[] dataCallback) {

        Message message = messageService.returnCallback(dataCallback);
        messageService.save(message);
    }

    /**
     *
     * @return String
     */
    @RequestMapping(method = RequestMethod.POST, value = "/longscheduling")
    @ResponseBody
    public String sendLongScheduling(@RequestBody MessageDTO messageDto) {

        if (messageService.thereIsNotAccount(messageDto.getIdDomain(), messageDto.getIdAccountClient())) {
            return RETURN_NOK;
        }

        Message entity = messageService.getMessageInstance(messageDto);
        entity.setTypeSend(TypeSendEnum.LONG_CODE);

        String[] data = { entity.getMobileNumber(), entity.getBody(),
                dateTimeUtil.convertDateTimeToStringFormatDDMMYYYY(entity.getJobDateTime()),
                dateTimeUtil.convertDateTimeToStringFormatHHmm(entity.getJobDateTime())
        };
        Sms sendLongScheduling = SEND_LONG_SCHEDULING.sendMessage(data);

        return sendLongScheduling.returnSendSms(entity);
    }

    /**
     *
     * @param mobileNumber
     * @param text
     * @param idDomain
     * @param idAccount
     * @return String
     */
    @RequestMapping(method = RequestMethod.POST, value = "/longscheduling/{mobileNumber}/{text}/{dateTime}/{idDomain}/{idEvent}/{idAccount}")
    @ResponseBody
    public String sendLongScheduling(@PathVariable("mobileNumber") String mobileNumber,
                                     @PathVariable("text") String text,
                                     @PathVariable("dateTime") DateTime dateTime,
                                     @PathVariable("idDomain") Long idDomain,
                                     @PathVariable("idEvent") Long idEvent,
                                     @PathVariable("idAccount") Long idAccount) {

        if (messageService.thereIsNotAccount(idDomain, idAccount)) {
            return RETURN_NOK;
        }

        Message entity = messageService.getMessageInstance(mobileNumber, text, dateTime);
        entity.setIdDomain(idDomain);
        entity.setIdAccountClient(idAccount);
        entity.setIdEvent(idEvent);
        entity.setTypeSend(TypeSendEnum.LONG_CODE);

        String[] data = { entity.getMobileNumber(), entity.getBody(),
                dateTimeUtil.convertDateTimeToStringFormatDDMMYYYY(dateTime),
                dateTimeUtil.convertDateTimeToStringFormatHHmm(dateTime)
        };
        Sms sendLongScheduling = SEND_LONG_SCHEDULING.sendMessage(data);

        return sendLongScheduling.returnSendSms(entity);
    }

    /**
     *
     * @param messageDto
     * @return String
     */
    @RequestMapping(method = RequestMethod.POST, value = "/shortsimple")
    @ResponseBody
    public String sendShortSimple(@RequestBody MessageDTO messageDto) {

        if (messageService.thereIsNotAccount(messageDto.getIdDomain(), messageDto.getIdAccountClient())) {
            return RETURN_NOK;
        }

        Message entity = messageService.getMessageInstance(messageDto);
        entity.setTypeSend(TypeSendEnum.SHORT_CODE);

        String[] data = { entity.getMobileNumber(), entity.getBody() };
        Sms sendLongSimple = SEND_SHORT_SIMPLE.sendMessage(data);

        return sendLongSimple.returnSendSms(entity);
    }

    /**
     *
     * @param mobileNumber
     * @param text
     * @param idDomain
     * @param idAccount
     * @return String
     */
    @RequestMapping(method = RequestMethod.POST, value = "/shortsimple/{mobileNumber}/{text}/{idDomain}/{idEvent}/{idAccount}")
    @ResponseBody
    public String sendShortSimple(@PathVariable("mobileNumber") String mobileNumber,
                                  @PathVariable("text") String text,
                                  @PathVariable("idDomain") Long idDomain,
                                  @PathVariable("idEvent") Long idEvent,
                                  @PathVariable("idAccount") Long idAccount) {

        if (messageService.thereIsNotAccount(idDomain, idAccount)) {
            return RETURN_NOK;
        }

        Message entity = messageService.getMessageInstance(mobileNumber, text, null);
        entity.setIdDomain(idDomain);
        entity.setIdAccountClient(idAccount);
        entity.setIdEvent(idEvent);
        entity.setTypeSend(TypeSendEnum.SHORT_CODE);

        String[] data = { entity.getMobileNumber(), entity.getBody() };
        Sms sendLongSimple = SEND_SHORT_SIMPLE.sendMessage(data);

        return sendLongSimple.returnSendSms(entity);
    }


    /**
     *
     * @return String
     */
    @RequestMapping(method = RequestMethod.POST, value = "/shortcheduling")
    @ResponseBody
    public String sendShortScheduling(@RequestBody MessageDTO messageDto) {

        if (messageService.thereIsNotAccount(messageDto.getIdDomain(), messageDto.getIdAccountClient())) {
            return RETURN_NOK;
        }

        Message entity = messageService.getMessageInstance(messageDto);
        entity.setTypeSend(TypeSendEnum.SHORT_CODE);

        String[] data = { entity.getMobileNumber(), entity.getBody()};
        Sms sendLongScheduling = SEND_SHORT_SCHEDULING.sendMessage(data);

        return sendLongScheduling.returnSendSms(entity);
    }

    /**
     *
     * @param messageDto
     * @return String
     */
    @RequestMapping(method = RequestMethod.POST, value = "/shortcallback")
    @ResponseBody
    public String sendShortCallBack(@RequestBody MessageDTO messageDto) {

        if (messageService.thereIsNotAccount(messageDto.getIdDomain(), messageDto.getIdAccountClient())) {
            return RETURN_NOK;
        }

        Message entity = messageService.getMessageInstance(messageDto);
        entity.setTypeSend(TypeSendEnum.SHORT_CODE);

        String[] data = { entity.getMobileNumber(), entity.getBody() };
        Sms sendShortCallBack = SEND_SHORT_CALL_BACK.sendMessage(data);

        return sendShortCallBack.returnSendSms(entity);
    }

    /**
     Parâmetros contidos no data_callback:
     codigo_campanha; celular; resposta; data_resposta; status; interno_key

     [0] codigo_campanha = Código da campanha retornado pela função sendsms (Tipo de dados Inteiro)
     [1] celular = Número do celular de retonro (Tipo de dados String)
     [2] resposta = Mensagem SMS retornada (Tipo de Dados String)
     [3] data_resposta = Data de recebimento da mensagem (Tipo de Dados String - Formato: yyyyMM-dd HH:mm:ss)
     [4] status = Status da entrega (Tipo de Dados String)
     [5] interno_key = Código de referência passado pela função sendsms (Tipo de Dados String)

     * @param dataCallback
     * @return String
     */
    @RequestMapping(method = RequestMethod.POST, value = "/returnshortcallback/{data_callback}")
    @ResponseBody
    public void returnShortCallBack(@PathVariable("data_callback") Object[] dataCallback) {

        Message message = messageService.returnCallback(dataCallback);
        messageService.save(message);
    }

    /**
     *
     * @param mobileNumber
     * @param text
     * @param idDomain
     * @param idAccount
     * @return String
     */
    @RequestMapping(method = RequestMethod.POST, value = "/shortcallback/{mobileNumber}/{text}/{idDomain}/{idAccount}")
    @ResponseBody
    public String sendShortCallBack(@PathVariable("mobileNumber") String mobileNumber,
                                    @PathVariable("text") String text,
                                    @PathVariable("idDomain") Long idDomain,
                                    @PathVariable("idAccount") Long idAccount) {

        if (messageService.thereIsNotAccount(idDomain, idAccount)) {
            return RETURN_NOK;
        }

        Message entity = messageService.getMessageInstance(mobileNumber, text, null);
        entity.setIdDomain(idDomain);
        entity.setIdAccountClient(idAccount);
        entity.setTypeSend(TypeSendEnum.SHORT_CODE);

        String[] data = { entity.getMobileNumber(), entity.getBody() };
        Sms sendShortCallBack = SEND_SHORT_CALL_BACK.sendMessage(data);

        return sendShortCallBack.returnSendSms(entity);
    }



    /**
     *
     * @param mobileNumber
     * @param text
     * @param idDomain
     * @param idAccount
     * @return String
     */
    @RequestMapping(method = RequestMethod.POST, value = "/shortscheduling/{mobileNumber}/{text}/{dateTime}/{idDomain}/{idAccount}")
    @ResponseBody
    public String sendShortSchedulingApi(@PathVariable("mobileNumber") String mobileNumber,
                                         @PathVariable("text") String text,
                                         @PathVariable("dateTime") DateTime dateTime,
                                         @PathVariable("idDomain") Long idDomain,
                                         @PathVariable("idAccount") Long idAccount) {

        if (messageService.thereIsNotAccount(idDomain, idAccount)) {
            return RETURN_NOK;
        }

        Message entity = messageService.getMessageInstance(mobileNumber, text, dateTime);
        entity.setIdDomain(idDomain);
        entity.setIdAccountClient(idAccount);
        entity.setTypeSend(TypeSendEnum.SHORT_CODE);
        String[] data = { entity.getMobileNumber(), entity.getBody(),
                dateTimeUtil.convertDateTimeToStringFormatDDMMYYYY(dateTime),
                dateTimeUtil.convertDateTimeToStringFormatHHmm(dateTime)
        };
        Sms sendShortScheduling = SEND_SHORT_SCHEDULING_API.sendMessage(data);

        return sendShortScheduling.returnSendSms(entity);
    }

    /**
     * Method responsible for balance access service related to long code account
     *
     * TODO Obs.: Esse comando só poderá ser solicitado de 1 em 1 minuto.
     * @return String
     */
    @RequestMapping(method = RequestMethod.GET, value = "/balancelong")
    public String getLongCurrentBalance() {

        Sms longCurrentBalance = GET_LONG_CURRENT_BALANCE.sendMessage(null);
        return longCurrentBalance.returnSendSms(null);
    }

    /**
     * Method responsible for balance access service related to short code account
     *
     * TODO Obs.: Esse comando só poderá ser solicitado de 1 em 1 minuto.
     * @return String
     */
    @RequestMapping(method = RequestMethod.GET, value = "/balanceshort")
    public String getShortCurrentBalance() {

        Sms shortCurrentBalance = GET_SHORT_CURRENT_BALANCE.sendMessage(null);
        return shortCurrentBalance.returnSendSms(null);
    }

    @Override
    public MessageService getService() {
        return this.messageService;
    }

    @Override
    public String[] getEditRoles() {
        return new String[]{"ROLE_ADMIN"};
    }

    @Override
    public String[] getReadRoles() {
        return new String[]{"ROLE_ADMIN"};
    }
}
