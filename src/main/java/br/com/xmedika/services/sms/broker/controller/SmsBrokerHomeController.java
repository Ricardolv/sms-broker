package br.com.xmedika.services.sms.broker.controller;


import br.com.socialpro.component.MessageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class SmsBrokerHomeController extends br.com.socialpro.controller.AbstractHomeController {

    @Autowired
    private MessageUtil messageUtil;

    @RequestMapping(value = {"/login"})
    @Override
    public String login(@RequestParam(value = "error", required = false) String error,
                        @RequestParam(value = "logout", required = false) String logout, Model model) {
        if (error != null) {
            model.addAttribute("error", messageUtil.getMessage("security.user.signin.invalid.data"));
        }

        if (logout != null) {
            model.addAttribute("msg", messageUtil.getMessage("security.user.signout.success"));
        }

        return "login";
    }
}