package br.com.xmedika.services.sms.broker.model;

/**
 * On the other hand, we can make the class final and hence non-extendable.
 * Moreover we can make the constructor private hence this class is non-instantiable.
 * Since the new class is non-instantiable, it will never break the contract.
 * Also, if a particular constant is being used multiple times in the same class,
 * the developer might make use of static import.
 */
public class SmsConstants {

    //Hide the constructor
    private SmsConstants() {

    }

    public static Integer DEBIT_A_CREDIT = 1;

    public static String RETURN_OK = "OK";
    public static String RETURN_NOK = "NOK";

    public static String USER_NAME = "31994948499";
    public static String DESC_LGN = "lgn";
    public static String DESC_PASSWORD = "pwd";

    public static String HTTP = "http://";
    public static String HOST = "54.173.24.177";

    public static String DESC_ACTION = "action";
    public static String SEND_MESSAGE = "sendsms";
    public static String GET_BALANCE = "getbalance";

    public static String DESC_ID_CAMPANHA = "id";
    public static String GET_STATUS_CAMPANHA = "getstatus";

    public static String DESC_MESSAGE = "msg";
    public static String DESC_NUMBERS = "numbers";
    public static String DESC_URL_CALLBACK = "url_callback";

    public static String DESC_JOB_DATE = "jobdate";
    public static String DESC_JOB_TIME = "jobtime";

    /*LOCA SMS*/
    public static String LOCA_SMS_PASSWORD = "922354";
    public static String LOCA_SMS_PATH = "/painel/api.ashx";
    public static String LOCA_SMS_URL_CALLBACK = "http://192.168.100.6:9000/message/longcallback/{data_callback}";

    /*PLATAFORMA SMS*/
    public static String PLATAFORM_SMS_PASSWORD = "678877";
    public static String PLATAFORM_SMS_PATH = "/shortcode/api.ashx";
    public static String PLATAFORM_SMS_PATH_OTHER = "/shortcode/ServiceSms.asmx";
    public static String PLATAFORM_SMS_URL_CALLBACK = "http://192.168.100.6:9000/message/shortcallback/{data_callback}";

    /*IMED*/
    public static String ENDPOINT_IMED_CUSTOMER_RETURN = "http://192.168.100.6:8080/customer/return";
    public static String ENDPOINT_IMED_CUSTOMER_ALERT = "http://192.168.100.6:8080/customer/alert";
}
