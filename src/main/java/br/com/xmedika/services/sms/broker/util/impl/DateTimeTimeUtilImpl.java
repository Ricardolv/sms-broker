package br.com.xmedika.services.sms.broker.util.impl;

import br.com.xmedika.services.sms.broker.util.DateTimeUtil;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.stereotype.Component;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class DateTimeTimeUtilImpl implements DateTimeUtil {

    public static final String YYYY_MM_DD = "yyyy-MM-dd";
    public static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
    public static final String DD_MM_YYYY_HH_MM_SS = "dd/MM/yyyy HH:mm:ss";

    private static Time timeUTCTime(Time time) {
        return new Time(new DateTime(time, DateTimeZone.UTC).getMillis());
    }

    public String convertToYYYYMMDD(Date date) {
        if (null != date) {
            DateFormat dateFormat = new SimpleDateFormat(YYYY_MM_DD);
            return dateFormat.format(date);
        } else {
            return "";
        }
    }

    /**
     * Metodo responsavel por converter String para Date no formato yyyy-MM-dd HH:mm:ss
     * @param inputDate
     * @return Date
     */
    @Override
    public Date convertStringToDateFormatYMDHMS(String inputDate) {
        return convertStringToDate(inputDate, YYYY_MM_DD_HH_MM_SS);
    }

    /**
     * Metodo responsavel por converter String para Date no formato dd/MM/yyyy HH:mm:ss
     * @param inputDate
     * @return Date
     */
    @Override
    public Date convertStringToDateFormatDMYHMS(String inputDate) {
        return convertStringToDate(inputDate, DD_MM_YYYY_HH_MM_SS);
    }

    /**
     * Metodo responsavel por converter String para Date no formato dd/MM/yyyy HH:mm:ss
     * @param date
     * @param time
     * @return Date
     */
    @Override
    public DateTime convertStringToDateFormatDMYHMS(String date, String time) {
        String inputDateTime = date + " " + time;
        return new DateTime(convertStringToDate(inputDateTime, DD_MM_YYYY_HH_MM_SS));
    }


    private Date convertStringToDate(String date, String fomart) {
        if (null != date && !date.isEmpty()) {
            DateFormat dateFormat = new SimpleDateFormat(fomart);
            try {
                return dateFormat.parse(date);
            } catch (ParseException e) {
                return null;
            }
        }
        return null;
    }

    public String convertDateTimeToStringFormatDDMMYYYY(DateTime dateTime) {
        String date = null != dateTime ?
                dateTime.toDate().getDate() + "/" +  dateTime.getMonthOfYear() + "/" + dateTime.getYear() : " ";
        return date;
    }

    public String convertDateTimeToStringFormatHHmm(DateTime dateTime) {
        String time = null != dateTime ?
                dateTime.toDate().getHours() + ":" + dateTime.toDate().getMinutes() : "";
        return time;
    }

}
