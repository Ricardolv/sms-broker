package br.com.xmedika.services.sms.broker.config.jobs.messagelog;

import br.com.socialpro.config.BatchConfig;
import br.com.socialpro.transaction.SPTransactionManager;
import br.com.xmedika.services.sms.broker.tasklet.SendMessageLogTasklet;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.interceptor.DefaultTransactionAttribute;

@Configuration
@Import({BatchConfig.class})
public class SendMessageLogConfig {

    @Autowired
    private JobBuilderFactory jobs;

    @Autowired
    private StepBuilderFactory steps;

    @Autowired
    private SendMessageLogTasklet sendMessageLogTasklet;

    @Autowired
    private SPTransactionManager spTransactionManager;

    @Bean
    @Qualifier(value = "sendMessageLogJob")
    public Job sendMessageLogJob() {
        return jobs.get("sendMessageLogJob")
                .start(stepSendMessageLogStep())
                .build();
    }

    @Bean
    public Step stepSendMessageLogStep() {
        DefaultTransactionAttribute transactionAttribute = new DefaultTransactionAttribute();

        transactionAttribute.setIsolationLevel(TransactionDefinition.ISOLATION_DEFAULT);
        transactionAttribute.setPropagationBehavior(TransactionDefinition.PROPAGATION_NOT_SUPPORTED);
        transactionAttribute.setTimeout(30);

        return steps.get("sendMessageLogJobStep")
                .transactionManager(spTransactionManager)
                .tasklet(sendMessageLogTasklet)
                .transactionAttribute(transactionAttribute)
                .build();
    }


}
